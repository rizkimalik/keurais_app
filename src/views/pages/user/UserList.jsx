import React from 'react'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import DataGrid, { Column, FilterRow, GroupPanel, HeaderFilter, MasterDetail, Pager, Paging } from 'devextreme-react/data-grid';

import UserDetail from './UserDetail';
import Icons from 'views/components/Icons';
import SplashScreen from 'views/components/SplashScreen';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonExport, ButtonRefresh } from 'views/components/button';
import { useGetUsersQuery, useDeleteUserMutation } from 'app/services/apiUser';
import { authUser } from 'app/slice/sliceAuth';
import { apiKickLoginAuth } from 'app/services/apiAuth';
import { SwalAlertSuccess } from 'views/components/SwalAlert';

function UserList() {
    const dispatch = useDispatch();
    const { user_level } = useSelector(authUser);
    const { data, isFetching, refetch } = useGetUsersQuery();
    const [deleteUser] = useDeleteUserMutation();

    async function deleteUserHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const res = await deleteUser(id)
                const data = res.data.data;
                refetch();

                Swal.fire(
                    data.message,
                    "Your data has been deleted.",
                    "success"
                )
            }
        });
    }

    async function KickLoginHandler(username) {
        Swal.fire({
            title: "Are you sure?",
            text: "You want kick out this User Login!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, kick it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiKickLoginAuth({ username }));
                if (payload.status === 200) {
                    SwalAlertSuccess('Success Kick Out.', `User data has been Logout.`);
                    refetch();
                }
            }
        });
    }

    function onExportExcel() {
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet('Main sheet');
        worksheet.columns = [
            { header: 'Username', key: 'username' },
            { header: 'Name', key: 'name' },
            { header: 'Email', key: 'email_address' },
            { header: 'Level', key: 'user_level' },
        ]
        worksheet.addRows(data.data);
        worksheet.autoFilter = 'A1:D1';
        worksheet.eachRow(function (row, rowNumber) {
            row.eachCell((cell, colNumber) => {
                if (rowNumber === 1) {
                    // First set the background of header row
                    cell.fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'f5b914' }
                    }
                }
                // Set border of each cell 
                cell.border = {
                    top: { style: 'thin' },
                    left: { style: 'thin' },
                    bottom: { style: 'thin' },
                    right: { style: 'thin' }
                };
            })
            row.commit();
        });

        workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'ExcelUsersGrid.xlsx');
        });
    }

    function componentButtonActions(data) {
        const { id, username, user_level } = data.row.data;
        return (
            <div className={`d-flex align-items-end justify-content-center ${user_level === 'Admin' ? 'hide' : ''}`}>
                <ButtonEdit to={`user/${id}/edit`} />
                        <ButtonDelete onClick={() => deleteUserHandler(id)} />
                <button onClick={(e) => KickLoginHandler(username)} type="button" className="btn btn-icon btn-light-info btn-hover-info btn-sm mx-1" data-toggle="tooltip" title="Kick Out Login">
                    <Icons iconName="sign-out" className="svg-icon svg-icon-sm svg-icon-info" />
                </button>
            </div>
        )
    }

    return (
        <MainContent>
            <SubHeader active_page="Settings" menu_name="Management User" modul_name="">
                <ButtonExport onClick={() => onExportExcel()} />
                <ButtonCreate to="/user/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Management User" subtitle="User login system or interface designed for users with administrative or managerial roles within an organization." />
                        <CardToolbar>
                            <ButtonRefresh onClick={(e) => refetch()} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        {isFetching && <SplashScreen />}
                        <DataGrid
                            dataSource={data?.data}
                            keyExpr="id"
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <MasterDetail
                                enabled={true}
                                component={UserDetail}
                            />
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <GroupPanel visible={true} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50, 'all']}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            {
                                (user_level === 'Admin' || user_level === 'SPV') &&
                                <Column caption="Actions" dataField="id" width={150} cellRender={componentButtonActions} />
                            }
                            <Column caption="Username" dataField="username" />
                            <Column caption="Name" dataField="name" />
                            <Column caption="Level" dataField="user_level" />
                            <Column caption="Department" dataField="department_name" />
                            <Column caption="Email" dataField="email_address" />
                            <Column caption="Aux" dataField="aux_name" cellRender={(data) => {
                                return data.value === 'Ready' ? <span className="text-success">{data.value}</span> : <span className="text-warning">{data.value}</span>;
                            }} />
                            <Column caption="Status" dataField="login" cellRender={(data) => {
                                return data.value === 1 ? <span className="text-success">Online</span> : <span className="text-danger">Offline</span>;
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default UserList
