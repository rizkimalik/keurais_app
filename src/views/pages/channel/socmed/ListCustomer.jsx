import React from 'react'
import { useDispatch, useSelector } from 'react-redux';

import IconBrand from 'views/components/IconBrand';
import DurationTimer from 'views/components/DurationTimer';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { authUser } from 'app/slice/sliceAuth';
import { ButtonRefresh } from 'views/components/button';
import { DatetimeFormat } from 'views/components/Datetime';

function ListCustomer({ handlerSelectCustomer, getListCustomer }) {
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { list_customers, status, selected_customer } = useSelector(state => state.sosialmedia);

    return (
        <Card>
            <CardHeader className="p-4">
                <CardTitle title={username} subtitle={status.socket_id !== null ? <span className="text-success">Available</span> : 'Not Ready'} />
                <CardToolbar>
                    <span className="font-weight-bold font-size-sm mr-2">Live</span>
                    <span className="label label-rounded label-light-primary">{list_customers ? list_customers.length : 0}</span>
                    <ButtonRefresh onClick={() => dispatch(getListCustomer({ agent_handle: username }))} />
                </CardToolbar>
            </CardHeader>
            <CardBody className="p-0">
                <div style={{ height: 'calc(75vh - 60px)', overflow: 'auto' }}>
                    {
                        list_customers?.map((customer, index) => {
                            return (
                                <div className="list list-hover border-bottom" key={index} onClick={() => handlerSelectCustomer(customer)}>
                                    <div className={`list-item d-flex align-items-center justify-content-between ${customer.chat_id === selected_customer?.chat_id ? 'active' : ''}`}>
                                        <div className="d-flex align-items-center w-50 py-4 mx-2">
                                            <div className="symbol symbol-45px symbol-circle">
                                                <div className="symbol-label fw-bolder">
                                                    {
                                                        customer.channel === 'Facebook_Messenger'
                                                            ? <IconBrand name={(customer.channel).split('_')[1]} height={45} width={45} />
                                                            : <IconBrand name={(customer.channel).split('_')[0]} height={45} width={45} />
                                                    }
                                                    {/* <IconBrand name={(customer.channel).split('_')[0]} height={45} width={45} /> */}
                                                </div>
                                            </div>
                                            <div className="flex-grow-1 mx-2">
                                                <div className="mr-2 text-truncate" style={{ maxWidth: '150px' }}>{customer.name}</div>
                                                <div className="text-muted my-1">{customer.agent_handle ? customer.agent_handle : '-'}</div>
                                                <div className="font-size-xs text-muted">{DatetimeFormat(customer.last_time)}</div>
                                            </div>

                                        </div>
                                        <div className="d-flex flex-column align-items-end mx-2">
                                            <div className="d-flex align-items-center justify-content-between mt-2">
                                                <span className="label label-light-primary font-weight-bold label-inline mx-1">{customer.channel}</span>
                                                <span className={`label label-sm label-rounded mx-1 ${customer.total_chat > 0 ? 'label-danger' : 'label-success'}`}>
                                                    {customer.total_chat}
                                                </span>
                                            </div>
                                            <span className="text-muted mt-2 font-size-sm">{customer.page_name}</span>
                                            <DurationTimer duration={customer.handling_duration} />
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </CardBody>
        </Card>
    )
}

export default ListCustomer