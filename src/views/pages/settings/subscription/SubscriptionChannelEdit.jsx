import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom';

import FormInput from 'views/components/FormInput'
import DataAssignedDepartment from './DataAssignedDepartment';
import ModalAssignDepartment from './ModalAssignDepartment';
import { ButtonSubmit, ButtonCancel } from 'views/components/button';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiSubcribedChannelDetail, apiSubcribedChannelUpdate } from 'app/services/apiSubscription';
import { Card, CardHeader, CardBody, CardTitle, CardFooter, CardToolbar } from 'views/components/card'
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';

function SubscriptionChannelEdit() {
    const dispatch = useDispatch();
    const search = useLocation().search;
    const { subcribed_channel_detail } = useSelector(state => state.subscription);
    const [detail_channel, setDetailChannel] = useState({});
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        const id = new URLSearchParams(search).get('id');
        const page_id = new URLSearchParams(search).get('page_id');
        const channel = new URLSearchParams(search).get('channel');
        setDetailChannel({ id, page_id, channel });
        dispatch(apiSubcribedChannelDetail({ id, page_id, channel }));
    }, [dispatch, search]);

    useEffect(() => {
        const channel = new URLSearchParams(search).get('channel');
        if (channel === 'Email') {
            reset({
                id: subcribed_channel_detail?.id,
                username: subcribed_channel_detail?.username,
                password: subcribed_channel_detail?.password,
                host: subcribed_channel_detail?.host,
                port: subcribed_channel_detail?.port,
                tls: subcribed_channel_detail?.tls,
                type: subcribed_channel_detail?.type,
                active: subcribed_channel_detail?.active,
            });
        }
        else {
            reset({
                id: subcribed_channel_detail?.id,
                page_id: subcribed_channel_detail?.page_id,
                page_name: subcribed_channel_detail?.page_name,
                channel: subcribed_channel_detail?.channel,
                account_id: subcribed_channel_detail?.account_id,
                url_api: subcribed_channel_detail?.url_api,
                token: subcribed_channel_detail?.token,
                token_secret: subcribed_channel_detail?.token_secret,
                active: subcribed_channel_detail?.active,
            });
        }
    }, [subcribed_channel_detail, search, reset]);

    const onSubmitUpdateChannel = async (data) => {
        try {
            const { payload } = await dispatch(apiSubcribedChannelUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Success.', 'Updated data channel!')
            }
            else if (payload.status === 201) {
                SwalAlertError('Already Exists.', payload.data);
            }
        }
        catch (error) {
            SwalAlertError('Failed Update.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Setting" menu_name="Detail Channel" modul_name="Activation account channel sosial media." />
            <Container>
                <ModalAssignDepartment detail_channel={detail_channel} />

                <div className="row">
                    <div className="col-lg-6">
                        <Card>
                            <CardHeader>
                                <CardTitle title="Detail Channel" subtitle="Data account channel sosial media." />
                                <CardToolbar>
                                    <button type="button" className="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalAssignDepartment">
                                        Assigned Department
                                    </button>
                                </CardToolbar>
                            </CardHeader>
                            {
                                detail_channel.channel === 'Email'
                                    ? <form onSubmit={handleSubmit(onSubmitUpdateChannel)} className="form">
                                        <CardBody>
                                            <input type="text" className="hide" {...register("id", { required: true })} />
                                            <input type="text" className="hide" {...register("channel", { required: true })} value="Email" />
                                            <FormInput
                                                name="username"
                                                type="email"
                                                label="Email"
                                                className="form-control"
                                                placeholder="Enter Email Server"
                                                register={register}
                                                rules={{ required: true, pattern: /^\S+@\S+$/i }}
                                                readOnly={false}
                                                errors={errors.username}
                                            />
                                            <FormInput
                                                name="password"
                                                type="password"
                                                label="Password"
                                                className="form-control"
                                                placeholder="Enter Password"
                                                register={register}
                                                rules={{ required: true, maxLength: 100 }}
                                                readOnly={false}
                                                errors={errors.password}
                                            />
                                            <FormInput
                                                name="port"
                                                type="number"
                                                label="Port"
                                                className="form-control"
                                                placeholder="Enter Port"
                                                register={register}
                                                rules={{ pattern: /^[0-9]+$/i }}
                                                readOnly={false}
                                                errors={errors.port}
                                            />
                                            <FormInput
                                                name="tls"
                                                type="number"
                                                label="TLS"
                                                className="form-control"
                                                placeholder="Enter TLS"
                                                register={register}
                                                rules={{ pattern: /^[0-9]+$/i }}
                                                readOnly={false}
                                                errors={errors.tls}
                                            />
                                            <FormInput
                                                name="host"
                                                type="text"
                                                label="Host Domain"
                                                className="form-control"
                                                placeholder="Enter Host Domain"
                                                register={register}
                                                rules={{ required: true }}
                                                readOnly={false}
                                                errors={errors.host}
                                            />
                                            <div className="row mb-4">
                                                <div className="col-lg-12">
                                                    <label>Email Type:</label>
                                                    <select className="form-control" {...register("type", { required: true })}>
                                                        <option value="Inbound">Inbound</option>
                                                        <option value="Outbound">Outbound</option>
                                                    </select>
                                                    {errors.type && <span className="form-text text-danger">Select enter Type</span>}
                                                </div>
                                            </div>
                                            <FormInput
                                                name="active"
                                                type="checkbox"
                                                label="Active"
                                                className="switch switch-outline switch-icon switch-primary ml-4"
                                                register={register}
                                                rules=""
                                                readOnly={false}
                                                errors={errors.active}
                                            />
                                        </CardBody>
                                        <CardFooter>
                                            <ButtonCancel to="/setting/subscription" />
                                            <ButtonSubmit />
                                        </CardFooter>
                                    </form>

                                    // form untuk selain email
                                    : <form onSubmit={handleSubmit(onSubmitUpdateChannel)} className="form">
                                        <CardBody>
                                            <input type="text" className="hide" {...register("id", { required: true })} />
                                            <FormInput
                                                name="page_id"
                                                type="text"
                                                label="PageID"
                                                className="form-control"
                                                placeholder="Enter PageID"
                                                register={register}
                                                rules={{ required: true, maxLength: 200 }}
                                                readOnly={false}
                                                errors={errors.page_id}
                                            />
                                            <FormInput
                                                name="page_name"
                                                type="text"
                                                label="Page name"
                                                className="form-control"
                                                placeholder="Enter Page Name"
                                                register={register}
                                                rules={{ required: true, maxLength: 200 }}
                                                readOnly={false}
                                                errors={errors.page_name}
                                            />
                                            <div className="row mb-2">
                                                <div className="col-lg-12">
                                                    <label>Channel:</label>
                                                    <select className="form-control" {...register("channel", { required: true })}>
                                                        <option value="Whatsapp">Whatsapp</option>
                                                        <option value="Facebook">Facebook</option>
                                                        <option value="Instagram">Instagram</option>
                                                        <option value="Twitter">Twitter</option>
                                                        <option value="Telegram">Telegram</option>
                                                    </select>
                                                    {errors.channel && <span className="form-text text-danger">Select enter Channel</span>}
                                                </div>
                                            </div>
                                            <FormInput
                                                name="account_id"
                                                type="text"
                                                label="Account ID"
                                                className="form-control"
                                                placeholder="Enter Account ID"
                                                register={register}
                                                rules={{ required: true, maxLength: 200 }}
                                                readOnly={false}
                                                errors={errors.account_id}
                                            />
                                            {
                                                (detail_channel.channel === 'Whatsapp' || detail_channel.channel === 'Telegram') &&
                                                <FormInput
                                                    name="url_api"
                                                    type="text"
                                                    label="Base URL API"
                                                    className="form-control"
                                                    placeholder="Enter Base URL API"
                                                    register={register}
                                                    rules={{ required: true, }}
                                                    readOnly={false}
                                                    errors={errors.url_api}
                                                />
                                            }
                                            <FormInput
                                                name="token"
                                                type="textarea"
                                                label="Token Key"
                                                className="form-control"
                                                placeholder="Enter Token Key"
                                                register={register}
                                                rules={{ required: true, }}
                                                readOnly={false}
                                                errors={errors.token}
                                            />
                                            <FormInput
                                                name="token_secret"
                                                type="textarea"
                                                label="Secret Key"
                                                className="form-control"
                                                placeholder="Enter Secret Key"
                                                register={register}
                                                rules=""
                                                readOnly={false}
                                                errors={errors.token_secret}
                                            />
                                            <FormInput
                                                name="active"
                                                type="checkbox"
                                                label="Active"
                                                className="switch switch-outline switch-icon switch-primary ml-4"
                                                register={register}
                                                rules=""
                                                readOnly={false}
                                                errors={errors.active}
                                            />
                                        </CardBody>
                                        <CardFooter>
                                            <ButtonCancel to="/setting/subscription" />
                                            <ButtonSubmit />
                                        </CardFooter>
                                    </form>
                            }

                        </Card>
                    </div>

                    <div className="col-lg-6">
                        <DataAssignedDepartment detail_channel={detail_channel} />
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default SubscriptionChannelEdit