import { createSlice } from "@reduxjs/toolkit";
import { getCallHistoryData, getCallTotal } from 'app/services/apiCall'

const sliceCall = createSlice({
    name: "call",
    initialState: {
        call_history: [],
        total_call: [],
    },
    extraReducers: {
        [getCallHistoryData.fulfilled]: (state, action) => {
            state.call_history = action.payload
        },
        [getCallTotal.fulfilled]: (state, action) => {
            state.total_call = action.payload.data
        },
    },
});

export default sliceCall;