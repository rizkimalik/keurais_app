import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { apiCustomer_DataChannel } from 'app/services/apiCustomer';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Item, Lookup, Pager, Paging, Toolbar } from 'devextreme-react/data-grid';
import { ButtonRefresh } from 'views/components/button';
import { apiMasterChannel } from 'app/services/apiMasterData';

function CustomerDataChannel({ customer_id }) {
    const dispatch = useDispatch();
    const { customer_channel } = useSelector(state => state.customer);
    const { channels } = useSelector(state => state.master);

    useEffect(() => {
        dispatch(apiMasterChannel());
    }, [dispatch]);

    useEffect(() => {
        if (customer_id) {
            dispatch(apiCustomer_DataChannel({ customer_id }))
        }
    }, [dispatch, customer_id]);

    /* function componentButtonActions(data) {
        const { id } = data.row.data;
        return (
            <div className="d-flex align-items-end justify-content-center">
                <ButtonDelete onClick={(e) => alert(id)} />
            </div>
        )
    } */

    return (
        <div className="border rounded p-4">
            <div className="d-flex justify-content-between mb-5">
                <h5 className="font-weight-bolder">Data Channel</h5>
                <div>
                    <ButtonRefresh onClick={() => dispatch(apiCustomer_DataChannel({ customer_id }))} />
                </div>
            </div>
            <DataGrid
                dataSource={customer_channel}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={5} />
                <Pager
                    visible={true}
                    allowedPageSizes={[5, 10]}
                    displayMode='full'
                    showPageSizeSelector={true}
                    showInfo={true}
                    showNavigationButtons={true} />
                <Editing
                    refreshMode='reshape'
                    mode="form"
                    allowAdding={true}
                    allowDeleting={true}
                    allowUpdating={true}
                />
                {/* <Column caption="Actions" dataField="id" width={120} cellRender={componentButtonActions} /> */}
                {/* <Column caption="Customer ID" dataField="customer_id" allowEditing={false} visible={false} /> */}
                {/* <Column caption="Channel" dataField="flag_channel" /> */}
                <Column caption="Actions" type="buttons">
                    <Button name="delete" cssClass="btn btn-icon btn-light-danger btn-sm mx-1">
                            <i className="fas fa-ban icon-sm"></i>
                        </Button>
                        <Button name="edit" cssClass="btn btn-icon btn-light-warning btn-sm mx-1">
                            <i className="fas fa-edit icon-sm"></i>
                        </Button>
                </Column>
                <Column caption="ID" dataField="id" allowEditing={false} visible={false} />
                <Column dataField="flag_channel" caption="Channel">
                    <Lookup dataSource={[{ channel: 'Phone' }, ...channels]} valueExpr="channel" displayExpr="channel" />
                </Column>
                <Column caption="Value" dataField="value_channel" />
                <Column caption="Sync Origin" dataField="origin_id" allowEditing={false} />
                <Toolbar>
                    <Item name="addRowButton" showText="always" location="before" />
                </Toolbar>
            </DataGrid>
        </div>
    )
}

export default CustomerDataChannel