import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';

import DataGrid, { Column, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { ButtonCreate, ButtonDelete, ButtonEdit, ButtonRefresh } from 'views/components/button';
import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card';
import { apiStatusDelete, apiStatusList } from 'app/services/apiStatus';
import { SwalAlertSuccess } from 'views/components/SwalAlert';

function StatusList() {
    const dispatch = useDispatch();
    const { status } = useSelector(state => state.status)

    useEffect(() => {
        dispatch(apiStatusList())
    }, [dispatch]);

    async function deleteStatusHandler(id) {
        Swal.fire({
            title: "Are you sure?",
            text: "You wont be able to delete this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!"
        }).then(async function (res) {
            if (res.value) {
                const { payload } = await dispatch(apiStatusDelete({ id }));
                if (payload.status === 200) {
                    SwalAlertSuccess('Success Delete', `deleted from database!`);
                    dispatch(apiStatusList())
                }
            }
        });
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Status Ticket" modul_name="">
                <ButtonCreate to="/status/create" />
            </SubHeader>
            <Container>
                <Card>
                    <CardHeader className="border-0">
                        <CardTitle title="Status Ticket" subtitle="data status ticket." />
                        <CardToolbar>
                            <ButtonRefresh onClick={(e) => dispatch(apiStatusList())} />
                        </CardToolbar>
                    </CardHeader>
                    <CardBody>
                        <DataGrid
                            dataSource={status}
                            keyExpr="id"
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        >
                            <HeaderFilter visible={true} />
                            <FilterRow visible={true} />
                            <Paging defaultPageSize={10} />
                            <Pager
                                visible={true}
                                allowedPageSizes={[10, 20, 50]}
                                displayMode='full'
                                showPageSizeSelector={true}
                                showInfo={true}
                                showNavigationButtons={true} />
                            <Column caption="Actions" width={100} cellRender={({ data }) => {
                                return (
                                    <div className="d-flex align-items-end justify-content-center">
                                        <ButtonEdit to={`status/${data.id}/edit`} />
                                        <ButtonDelete onClick={(e) => deleteStatusHandler(data.id)} />
                                    </div>
                                )
                            }} />
                            <Column caption="Status Name" dataField="status" />
                            <Column caption="Description" dataField="description" />
                            <Column caption="Created By" dataField="created_by" />
                            <Column caption="Created At" dataField="created_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Updated By" dataField="updated_by" />
                            <Column caption="Updated At" dataField="updated_at" dataType="date" format="MM/dd/yyyy hh:mm" />
                            <Column caption="Active" dataField="active" width={100} cellRender={({ data }) => {
                                return (
                                    <label className={`font-weight-bold ${data.active ? 'text-success' : 'text-danger'}`}>
                                        {data.active ? 'Yes' : 'No'}
                                    </label>
                                )
                            }} />
                        </DataGrid>
                    </CardBody>
                </Card>
            </Container>
        </MainContent>
    )
}

export default StatusList