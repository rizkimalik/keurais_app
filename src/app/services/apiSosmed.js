import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const getListCustomer = createAsyncThunk(
    "sosialmedia/getListCustomer",
    async ({ agent_handle }) => {
        const res = await axios.post('/omnichannel/list_inbox_messages', { agent_handle });
        return res.data;
    }
)

export const getLoadConversation = createAsyncThunk(
    "sosialmedia/getLoadConversation",
    async (param) => {
        const res = await axios.post('/omnichannel/conversation_messages', JSON.stringify(param));
        return res.data;
    }
)

export const getEndChat = createAsyncThunk(
    "sosialmedia/getEndChat",
    async ({ chat_id, customer_id }) => {
        const res = await axios.post('/omnichannel/conversation_closed', { chat_id, customer_id });
        return res.data;
    }
)

export const getDataMessages = createAsyncThunk(
    "sosialmedia/getDataMessages",
    async (param) => {
        const store = new CustomStore({
            key: 'chat_id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/data_messages`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiInteractionDetail = createAsyncThunk(
    "sosialmedia/apiInteractionDetail",
    async (param) => {
        const res = await axios.post('/omnichannel/interaction_detail', JSON.stringify(param));
        return res.data;
    }
);

export const setAccountSpam = createAsyncThunk(
    "sosialmedia/setAccountSpam",
    async (params) => {
        const res = await axios.post('/omnichannel/set_account_spam', JSON.stringify(params));
        return res.data;
    }
)

export const apiChatTemplate = createAsyncThunk(
    "sosialmedia/apiChatTemplate",
    async () => {
        const res = await axios.post('/omnichannel/chat_templates').catch((error) => { return error.response });
        return res.data;
    }
)

export const apiTransferAssignAgent = createAsyncThunk(
    "sosialmedia/apiTransferAssignAgent",
    async (params) => {
        const res = await axios.post('/omnichannel/transfer_assign_agent', JSON.stringify(params));
        return res.data;
    }
)

export const apiAgentReadyAssign = createAsyncThunk(
    "sosialmedia/apiAgentReadyAssign",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/omnichannel/agent_ready_toassign`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

// feed modul
export const apiListInboxFeeds = createAsyncThunk(
    "sosialmedia/apiListInboxFeeds",
    async (params) => {
        const res = await axios.post('/omnichannel/list_inbox_feeds', JSON.stringify(params));
        return res.data;
    }
)

export const apiShowDetailFeed = createAsyncThunk(
    "sosialmedia/apiShowDetailFeed",
    async (params) => {
        const res = await axios.post('/omnichannel/show_detail_feed', JSON.stringify(params));
        return res.data;
    }
)

export const apiConversationFeed = createAsyncThunk(
    "sosialmedia/apiConversationFeed",
    async (params) => {
        const res = await axios.post('/omnichannel/conversation_feeds', JSON.stringify(params));
        return res.data;
    }
)
