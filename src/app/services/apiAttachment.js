import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiAttachment_TicketList = createAsyncThunk(
    "attachment/apiAttachment_TicketList",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/attachment/ticket/list`, params)
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { throw new Error(error); });
            },
        });

        return store;
    }
)

export const apiAttachment_TicketUpload = createAsyncThunk(
    "attachment/apiAttachment_TicketUpload",
    async (param) => {
        const formData = new FormData();
        formData.append('attachment_file', param.attachment_file[0]);
        formData.append('ticket_number', param.ticket_number);
        formData.append('created_by', param.created_by);
        formData.append('description', param.description);

        const config = {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }
        const result = await axios.post('/attachment/ticket/upload', formData, config);
        return result.data;
    }
)