import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiMasterChannel = createAsyncThunk(
    "master/apiMasterChannel",
    async () => {
        const res = await axios.get('/master/channel?data=active');
        return res.data;
    }
)

export const apiMasterChannelType = createAsyncThunk(
    "master/apiMasterChannelType",
    async ({ channel }) => {
        if (!channel) return;
        const res = await axios.get(`/master/channel_type?data=active&channel=${channel}`);
        return res.data;
    }
)

export const apiMasterStatus = createAsyncThunk(
    "master/apiMasterStatus",
    async () => {
        const res = await axios.get('/master/status?data=active');
        return res.data;
    }
)

export const apiMasterCustomerType = createAsyncThunk(
    "master/apiMasterCustomerType",
    async () => {
        const res = await axios.get('/customer_type?data=active');
        return res.data;
    }
)

export const apiMasterPriorityScale = createAsyncThunk(
    "master/apiMasterPriorityScale",
    async () => {
        const res = await axios.get('/priority_scale?data=active');
        return res.data;
    }
)

export const apiMasterUserLevel = createAsyncThunk(
    "master/apiMasterUserLevel",
    async () => {
        const res = await axios.get('/master/user_level');
        return res.data;
    }
)

export const apiMasterUserLayer = createAsyncThunk(
    "master/apiMasterUserLayer",
    async () => {
        const res = await axios.get('/master/user_level?data=ticketing');
        return res.data;
    }
)

export const apiMasterOrganization = createAsyncThunk(
    "master/apiMasterOrganization",
    async () => {
        const res = await axios.get('/organization?data=active');
        return res.data;
    }
)

export const apiMasterDepartment = createAsyncThunk(
    "master/apiMasterDepartment",
    async () => {
        const res = await axios.get('/department?data=active');
        return res.data;
    }
)


