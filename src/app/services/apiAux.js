import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const apiAuxData = createAsyncThunk(
    "aux/apiAuxData",
    async () => {
        const res = await axios.get('/aux');
        return res.data;
    }
)

export const apiAuxActive = createAsyncThunk(
    "aux/apiAuxActive",
    async () => {
        const res = await axios.get('/aux?data=active');
        return res.data;
    }
)

// export const apiStatusStore = createAsyncThunk(
//     "aux/apiStatusStore",
//     async (aux) => {
//         const json = JSON.stringify(aux);
//         const res = await axios.post('/aux/store', json);
//         return res.data;
//     }
// )

export const apiStatusAuxUser = createAsyncThunk(
    "aux/apiStatusAuxUser",
    async ({ username }) => {
        const res = await axios.get(`/aux/status_aux_user?username=${username}`);
        return res.data;
    }
)

export const apiUpdateAuxUser = createAsyncThunk(
    "aux/apiUpdateAuxUser",
    async (data) => {
        const res = await axios.put('/aux/update_aux_user', data);
        return res.data;
    }
)

// export const apiStatusDelete = createAsyncThunk(
//     "aux/apiStatusDelete",
//     async ({ id }) => {
//         const res = await axios.delete(`/aux/delete/${id}`);
//         return res.data;
//     }
// )
