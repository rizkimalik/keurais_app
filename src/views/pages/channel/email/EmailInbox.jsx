import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { authUser } from 'app/slice/sliceAuth';
import { ButtonRefresh } from 'views/components/button'
import { getEmailInboxData, apiInsertEmailSpam } from 'app/services/apiEmail';
import { IconMailOpen, IconMark, IconTicket, IconMailSpam } from 'views/components/icon';
import { Card, CardBody, CardHeader, CardTitle, CardToolbar } from 'views/components/card'
import { EmailDetail } from '.';
import { TransferAssignedAgent } from '../socmed';

function EmailInbox() {
    const dispatch = useDispatch();
    const user = useSelector(authUser);
    const [email_detail, setEmailDetail] = useState('');
    const { email_inbox } = useSelector(state => state.email);

    useEffect(() => {
        dispatch(getEmailInboxData({ agent_handle: user.username, user_level: user.user_level }))
    }, [dispatch, user]);

    /* function onCreateTicket(data) {
        history.push(`/ticket?thread_id=${data.EMAIL_ID}&channel=Email&account=${data.EFROM}`);
        history.go(0);
    } */

    function handlerEmailSpam({ email_id }) {
        Swal.fire({
            title: 'Action Email Spam.',
            text: 'Do you want to add email spam?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Spam',
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(apiInsertEmailSpam({ email_id }));
                dispatch(getEmailInboxData({ agent_handle: user.username, user_level: user.user_level }))
            }
        })
    }

    function componentButtonActions({ data }) {
        return (
            <div className="d-flex align-items-end justify-content-center">
                <NavLink to={`/ticket?thread_id=${data.email_id}&channel=Email&account=${data.efrom}`} className="btn btn-icon btn-light-primary btn-hover-primary btn-xs mx-1" data-toggle="tooltip" title="Create Ticket">
                    <IconTicket className="svg-icon svg-icon-xs" />
                </NavLink>
                <button type="button" onClick={() => setEmailDetail({ ...data, ...{ action: 'email_detail' } })} className="btn btn-icon btn-light-info btn-hover-info btn-xs mx-1" title="Detail Mail" data-toggle="modal" data-target="#modalEmailDetail">
                    <IconMailOpen className="svg-icon svg-icon-xs" />
                </button>
                <button type="button" onClick={() => setEmailDetail({ action: 'email_assign', current_agent: data.agent_handle, interaction_id: data.email_id })} className="btn btn-icon btn-light-success btn-hover-success btn-xs mx-1" title="Transfer Assigned Agent" data-toggle="modal" data-target="#modalTransferAssignedAgent">
                    <IconMark className="svg-icon svg-icon-xs" />
                </button>
                <button type="button" onClick={() => handlerEmailSpam({ email_id: data.email_id })} className="btn btn-icon btn-light-danger btn-hover-danger btn-xs mx-1" title="Spam Mail">
                    <IconMailSpam className="svg-icon svg-icon-xs" />
                </button>
                {/* <button type="button" onClick={() => setEmailDetail({ ...data, ...{ action: 'email_reply' } })} className="btn btn-icon btn-light-success btn-hover-success btn-xs mx-1" title="Reply Mail" data-toggle="modal" data-target="#modalEmailReply">
                    <IconMailReply className="svg-icon svg-icon-xs" />
                </button> */}
            </div>
        )
    }

    return (
        <div className="border-0">
            {email_detail.action === 'email_detail' && <EmailDetail email_detail={email_detail} />}
            {email_detail.action === 'email_assign' && <TransferAssignedAgent values={email_detail} />}
            {/* {email_detail.action === 'email_reply' && <EmailReply email_detail={email_detail} />} */}

            <Card>
                <CardHeader className="border-bottom">
                    <CardTitle title="Data Email Inbox" subtitle="Inbox data email has not yet been converted to a ticket." />
                    <CardToolbar>
                        <ButtonRefresh onClick={() => dispatch(getEmailInboxData({ agent_handle: user.username, user_level: user.user_level }))} />
                    </CardToolbar>
                </CardHeader>
                <CardBody className="p-4">
                    <DataGrid
                        dataSource={email_inbox}
                        remoteOperations={{
                            filtering: true,
                            sorting: true,
                            paging: true
                        }}
                        allowColumnReordering={true}
                        allowColumnResizing={true}
                        columnAutoWidth={true}
                        showBorders={true}
                        showColumnLines={true}
                        showRowLines={true}
                        columnWidth={150}
                    >
                        <HeaderFilter visible={true} />
                        <FilterRow visible={true} />
                        <Paging defaultPageSize={10} />
                        <Pager
                            visible={true}
                            allowedPageSizes={[10, 20, 50]}
                            displayMode='full'
                            showPageSizeSelector={true}
                            showInfo={true}
                            showNavigationButtons={true} />
                        <Column caption="Actions" dataField="id" fixed={true} width={150} cellRender={componentButtonActions} />
                        {/* <Column caption="Email ID" dataField="email_id" fixed={true} /> */}
                        <Column caption="Name" dataField="ename" fixed={true} />
                        <Column caption="Subject" dataField="esubject" />
                        <Column caption="Ticket Number" dataField="ticket_number" />
                        <Column caption="Agent" dataField="agent_handle" />
                        <Column caption="From" dataField="efrom" />
                        <Column caption="To" dataField="eto" />
                        <Column caption="Cc" dataField="ecc" />
                        <Column caption="Date Email" dataField="date_email" dataType="datetime" />
                        <Column caption="Date Receive" dataField="date_receive" dataType="datetime" />
                        <Column caption="Date Assign" dataField="date_blending" dataType="datetime" />
                    </DataGrid>
                </CardBody>
            </Card>
        </div>
    )
}

export default EmailInbox