import React, { memo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { apiEmailDataInteraction } from 'app/services/apiEmail';
import { IconMailIncoming, IconMailOutgoing } from 'views/components/icon';

function EmailDataInteraction({ data }) {
    const dispatch = useDispatch();
    const { email_interaction } = useSelector(state => state.email);

    useEffect(() => {
        if (!data.interaction_id) return;
        dispatch(apiEmailDataInteraction({ interaction_id: data.interaction_id }));
    }, [dispatch, data]);


    if (data) {
        //? load data interaction email by ticket_number
        if (data.interaction_id) {
            return (
                <div className="timeline timeline-3">
                    <div className="timeline-items">
                        {
                            email_interaction.map((item, index) => {
                                return <div className="timeline-item" key={index}>
                                    <div className="timeline-media">
                                        {
                                            item.direction === 'OUT'
                                                ? <IconMailOutgoing className="svg-icon svg-icon-lg svg-icon-danger" />
                                                : <IconMailIncoming className="svg-icon svg-icon-lg svg-icon-success" />
                                        }
                                    </div>
                                    <div className="timeline-content accordion accordion-light accordion-toggle-arrow border py-5" id={`accordion-${index}`}>
                                        <div className="collapsed" data-toggle="collapse" data-target={`#collapse-${index}`}>
                                            <div className="cursor-pointer toggle-on bg-white border-bottom" data-inbox="message">
                                                <div className="d-flex p-5 flex-row flex-md-row flex-lg-row flex-xxl-row justify-content-between">
                                                    <div className="d-flex align-items-center">
                                                        <div className="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                                                            <div className="d-flex">
                                                                <span className="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">{item.esubject}</span>
                                                            </div>
                                                            <div className="d-flex flex-column">
                                                                <div className="toggle-off-item">
                                                                    <span className="font-weight-bold text-muted cursor-pointer">
                                                                        From: {item.efrom}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="d-flex flex-column">
                                                                <div className="toggle-off-item">
                                                                    <span className="font-weight-bold text-muted cursor-pointer">
                                                                        To: {item.eto}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-center align-items-xxl-center flex-row flex-md-row flex-lg-row flex-xxl-row">
                                                        <div className="font-weight-bold text-muted mx-2">{(item.date_email).replace('T', ' ').substring(19, 0)}</div>
                                                        <div className="d-flex align-items-center flex-wrap flex-xxl-nowrap" data-inbox="toolbar">
                                                            <span className="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="Settings">
                                                                <i className="flaticon-more icon-1x" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id={`collapse-${index}`} className="collapse" data-parent={`#accordion-${index}`}>
                                            <div className="p-5 bg-white">
                                                <div dangerouslySetInnerHTML={{ __html: item.ebody_html }}></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            })
                        }
                    </div>
                </div>
            )
        }
        else {
            //? load initial data email
            return (
                <div className="timeline timeline-3">
                    <div className="timeline-items">
                        <div className="timeline-item">
                            <div className="timeline-media">
                                <i className="flaticon2-email text-success" />
                            </div>
                            <div className="timeline-content accordion accordion-light accordion-toggle-arrow border py-5" id="accordionExample1">
                                <div className="collapsed" data-toggle="collapse" data-target="#collapseThree1">
                                    <div className="cursor-pointer toggle-on bg-white border-bottom" data-inbox="message">
                                        <div className="d-flex p-5 flex-row flex-md-row flex-lg-row flex-xxl-row justify-content-between">
                                            <div className="d-flex align-items-center">
                                                <div className="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                                                    <div className="d-flex">
                                                        <span className="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">{data.esubject}</span>
                                                    </div>
                                                    <div className="d-flex flex-column">
                                                        <div className="toggle-off-item">
                                                            <span className="font-weight-bold text-muted cursor-pointer">
                                                                From: {data.efrom}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex flex-column">
                                                        <div className="toggle-off-item">
                                                            <span className="font-weight-bold text-muted cursor-pointer">
                                                                To: {data.eto}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="d-flex my-2 my-xxl-0 align-items-md-center align-items-lg-center align-items-xxl-center flex-row flex-md-row flex-lg-row flex-xxl-row">
                                                <div className="font-weight-bold text-muted mx-2">{(data.date_email).replace('T', ' ').substring(19, 0)}</div>
                                                <div className="d-flex align-items-center flex-wrap flex-xxl-nowrap" data-inbox="toolbar">
                                                    <span className="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="Settings">
                                                        <i className="flaticon-more icon-1x" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseThree1" className="collapse" data-parent="#accordionExample1">
                                    <div className="p-5 bg-white">
                                        <div dangerouslySetInnerHTML={{ __html: data.ebody_html }}></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
    else {
        return <div className="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
            <div className="alert-icon"><i className="flaticon-warning" /></div>
            <div className="alert-text">Interaction not found.</div>
        </div>
    }
}

export default memo(EmailDataInteraction);