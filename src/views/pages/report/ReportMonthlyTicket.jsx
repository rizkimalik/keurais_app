import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons'
import FormInput from 'views/components/FormInput';
import FormGroup from 'views/components/FormGroup';
import ModalFilterByCustomer from './ModalFilterByCustomer';
import { SwalAlertError } from 'views/components/SwalAlert';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiReportMonthlyTicket, apiReportMonthlyTicketExport } from 'app/services/apiReport';

function ReportMonthlyTicket() {
    const dispatch = useDispatch();
    const { report_monthly_ticket } = useSelector(state => state.report);
    const [navigate, setNavigate] = useState('');
    const [selected_customer, onSelectedCustomer] = useState('');
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let today = new Date();
        const date_start = new Date(new Date().setDate(new Date().getDate() - 30)).toISOString().slice(0, 10);
        const date_end = today.toISOString().slice(0, 10);
        setValue('date_start', date_start);
        setValue('date_end', date_end);
    }, [setValue]);

    useEffect(() => {
        setValue('customer_id', selected_customer.customer_id)
        setValue('customer_name', selected_customer.name)
    }, [setValue, selected_customer]);

    const onSubmitReportMonthlyTicket = async (data) => {
        try {
            const result = await dispatch(apiReportMonthlyTicket(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value,
                customer_id: document.querySelector('input[name="customer_id"]').value
            }
            const result = await dispatch(apiReportMonthlyTicketExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');
                worksheet.columns = [
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Start Date', key: 'start_date' },
                    { header: 'End Date', key: 'end_date' },
                    { header: 'Product', key: 'category_sublv2_name' },
                    { header: 'Priority', key: 'category_sublv3_name' },
                    { header: 'PIC', key: 'last_response_by' },
                    { header: 'Company', key: 'name' },
                    { header: 'Rised From', key: 'ticket_source' },
                    { header: 'Issue / Task', key: 'issue_task' },
                    { header: 'Root Cause', key: 'complaint_detail' },
                    { header: 'Solving Step', key: 'response_detail' },
                    { header: 'Status', key: 'status' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:W1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Report_Monthly_Ticket_${data.date_start}_${data.date_end}.xlsx`);
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Monthly Ticket" />
            <Container>
                {navigate === 'ModalFilterByCustomer' && <ModalFilterByCustomer onSelectedCustomer={onSelectedCustomer} />}
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Monthly Ticket</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportMonthlyTicket)} id="formReportMonthlyTicket">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-3">
                                <FormGroup label="Customer">
                                    <div className="input-group">
                                        <input type="text"
                                            {...register("customer_id", { required: false })}
                                            name="customer_id"
                                            className="form-control form-control-sm hide"
                                        />
                                        <input type="text"
                                            {...register("customer_name", { required: false })}
                                            name="customer_name"
                                            className="form-control form-control-sm"
                                            placeholder="Customer..."
                                            readOnly={true}
                                        />
                                        <div className="input-group-append">
                                            <button type="button" onClick={() => setNavigate('ModalFilterByCustomer')} className="btn btn-light-primary btn-sm" data-toggle="modal" data-target="#ModalFilterByCustomer">
                                                <Icons iconName="search" className="svg-icon svg-icon-sm" />
                                            </button>
                                        </div>
                                    </div>
                                </FormGroup>
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_monthly_ticket}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={false} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Ticket Number" dataField="ticket_number" />
                    <Column caption="Start Date" dataField="start_date" />
                    <Column caption="End Date" dataField="end_date" />
                    <Column caption="Product" dataField="category_sublv2_name" />
                    <Column caption="Priority" dataField="category_sublv3_name" />
                    <Column caption="PIC" dataField="last_response_by" />
                    <Column caption="Company" dataField="name" />
                    <Column caption="Rised From" dataField="ticket_source" />
                    <Column caption="Issue / Task" dataField="issue_task" />
                    <Column caption="Root Cause" dataField="complaint_detail" />
                    <Column caption="Solving Step" dataField="response_detail" />
                    <Column caption="Status" dataField="status" />
                </DataGrid>

            </Container>
        </MainContent>
    )
}

export default ReportMonthlyTicket