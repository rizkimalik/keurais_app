import React, { memo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { apiAuxActive, apiUpdateAuxUser, apiStatusAuxUser } from 'app/services/apiAux';
import { SwalAlertError, SwalNotifify } from 'views/components/SwalAlert';
import { useForm } from 'react-hook-form';
import { authUser } from 'app/slice/sliceAuth';

function PanelAux({ showAux, setShowAux }) {
    const dispatch = useDispatch();
    const { register, handleSubmit } = useForm();
    const { aux, aux_status } = useSelector(state => state.aux);
    const { username } = useSelector(authUser);

    useEffect(() => {
        dispatch(apiAuxActive());
        dispatch(apiStatusAuxUser({ username }));
    }, [dispatch, username]);

    const onSubmitAux = async (data) => {
        try {
            const { payload } = await dispatch(apiUpdateAuxUser(data))
            if (payload.status === 200) {
                dispatch(apiStatusAuxUser({ username }));
                SwalNotifify('AUX Status.', `Updated aux status ${payload.data}.`);
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Failed Update.', 'Please try again.');
        }
    }

    return (
        <div className={`offcanvas offcanvas-right p-10 ${showAux}`}>
            <div className="offcanvas-header d-flex align-items-center justify-content-between pb-7">
                <h4 className="font-weight-bold m-0">Select AUX</h4>
                <button onClick={() => setShowAux('')} className="btn btn-xs btn-icon btn-light btn-hover-primary">
                    <i className="ki ki-close icon-xs text-muted" />
                </button>
            </div>
            <div className="offcanvas-content">
                <form onSubmit={handleSubmit(onSubmitAux)} className="form">
                    <input type="hidden" name="username" value={username} {...register("username", { required: true })}/>
                    <select className="form-control my-5" name="aux" {...register("aux", { required: true })}>
                        {
                            aux.map((item, index) => {
                                let selected = item.id === aux_status.aux ? true : false;
                                return <option value={item.id} name={item.aux_name} key={index} selected={selected}>{item.aux_name}</option>
                            })
                        }
                    </select>

                    <div className="offcanvas-footer">
                        <button className="btn btn-block btn-success btn-shadow font-weight-bolder text-uppercase">Submit</button>
                    </div>
                </form>

                {/* <option data-icon="la la-bullhorn font-size-lg bs-icon" value="AZ">Arizona</option> */}
                {/* <select name="aux_status" className="form-control selectpicker my-5" data-size="5" data-live-search="true">
                    <option value="">-- select aux --</option>
                    {
                        aux.map((item, index) => {
                            return <option value={item.id} key={index}>{item.aux_name}</option>
                        })
                    }
                </select> */}
            </div>
        </div>
    )
}

export default memo(PanelAux)