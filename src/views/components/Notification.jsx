import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    type: "info",
    position: "bottom-end",
    timer: 5000,
    timerProgressBar: true,
    showCloseButton: true,
    showConfirmButton: false,
    width: 300,
    didOpen: (toast) => {
        toast.onmouseenter = Swal.stopTimer;
        toast.onmouseleave = Swal.resumeTimer;
    }
});

function AskPermission() {
    try {
        Notification.requestPermission().then((permission) => {
            console.log(`Notification permission : ${permission}`);
        });
    }
    catch (e) {
        return false;
    }
    return true;
}

function ShowNotification(data) {
    WebNotification(data);
    function PushNotification() {
        let options = {
            body: data.message,
            icon: '/assets/media/icons/notification.png',
            tag: data.message
        }
        const notif = new Notification(`${data.name} - ${data.channel}`, options);
        notif.onclick = function (event) {
            event.preventDefault();
            // location.href = `${DomainUrl}/inhealth/HTML/TrxShowTicket.aspx?ticketid=${Title}`;
        }
    }


    if (Notification.permission === 'granted') {
        PushNotification();
    } else if (Notification.permission !== 'denied') {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                PushNotification();
            }
        });
    }
}

function WebNotification(data) {
    const options = {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        hour12: false,
        timeZone: "Asia/Jakarta",
    };

    Toast.fire({
        title: `<strong>${data.channel} - ${data.name}</strong>`,
        html: `
            <p style="text-align: left; margin-top: 4px; width: 260px;">
                <b>${data.name}</b>, ${data.message}
            </p>
            <small>${new Intl.DateTimeFormat("en-GB", options).format(new Date())}</small>
        `,
    });

    // Swal.fire({
    //     title: `${data.name} - ${data.channel}`,
    //     text: data.message,
    //     icon: 'info',
    //     position: 'top-start',
    //     showConfirmButton: false,
    //     timer: 5000,
    //     toast: true,
    //     timerProgressBar: true
    // });
}


export { AskPermission, ShowNotification }