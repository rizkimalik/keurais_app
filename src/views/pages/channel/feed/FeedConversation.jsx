import React, { useState } from 'react';
import { FeedFormSend } from '.';

function FeedConversation({ conversation_feeds, user_level, selected_feed, message, setMessage, comment_id, setCommentID, sendReplyMessage }) {
    const [action_reply, setActionReply] = useState('');

    function clickReplyBtn(comment) {
        setMessage(''); // reset message
        setActionReply(action_reply ? '' : 'show');
        setCommentID(comment.comment_id);
        if (comment.channel === 'Instagram_Feed') {
            setMessage(`@${comment.name} `); // add mention @username
        }
    }

    function UserProfile({ data }) {
        if (data.flag_to === 'customer') {
            return <div className="symbol symbol-40 symbol-circle symbol-light-primary mr-5 mt-1">
                <span className="symbol-label font-size-lg font-weight-bold text-uppercase">{(data.name).slice(0, 1)}</span>
            </div>
        }
        else {
            return <div className="symbol symbol-40 symbol-circle symbol-light-info mr-5 mt-1">
                <span className="symbol-label font-size-lg font-weight-bold text-uppercase">
                    <i className="fas fa-headset text-info" />
                </span>
            </div>
        }
    }

    return (
        <div style={{ height: '400px', overflowY: 'auto' }}>
            {
                // if comment_id = message_id > comment level
                [...new Set(conversation_feeds)]?.filter((item) => item.chat_id === selected_feed.chat_id && item.comment_id === item.message_id).map((comment, index) => {
                    return <div className="d-flex pt-4" key={index}>
                        <UserProfile data={comment} />
                        <div className="d-flex flex-column flex-row-fluid">
                            <div className="d-flex align-items-center flex-wrap">
                                <span className="text-dark-75 mb-1 font-size-lg font-weight-bolder pr-6 flex-grow-1">{comment.name}</span>
                                <button type="button" onClick={() => clickReplyBtn(comment)} className="btn btn-sm btn-outline-secondary font-weight-normal font-size-sm">View Replies {[...new Set(conversation_feeds)]?.filter((item) => item.chat_id === selected_feed.chat_id && item.comment_id === comment.message_id && item.comment_id !== item.message_id).length}</button>
                            </div>
                            <span className="text-dark-75 font-size-sm font-weight-normal border-bottom pb-4">
                                {comment.message}
                                <div className="d-flex flex-column align-items-start mt-1">
                                    <div className="d-flex align-items-center justify-content-between">
                                        <span className="label label-sm label-rounded label-secondary" title={comment.flag_notif === 1 ? 'Read' : 'Unread'}>
                                            {
                                                comment.flag_notif === 1
                                                    ? <i className="fas fa-check-double icon-xs m-1 text-success" />
                                                    : <i className="fas fa-check icon-xs m-1" />
                                            }
                                        </span>
                                        <label className="text-muted font-size-xs m-1">{(comment.date_create).replace('T', ' ').slice(0, 19)}</label>
                                    </div>
                                </div>
                            </span>
                            {
                                action_reply === 'show' &&
                                comment_id === comment.comment_id &&
                                <div>
                                    {
                                        // show replies data comment, if reply.comment_id = comment.message_id > replies level
                                        [...new Set(conversation_feeds)]?.filter((item) => item.chat_id === selected_feed.chat_id && item.comment_id === comment.message_id && item.comment_id !== item.message_id)
                                            .map((reply, idx) => {
                                                return <div className="d-flex pt-4" key={idx}>
                                                    <UserProfile data={reply} />
                                                    <div className="d-flex flex-column flex-row-fluid">
                                                        <div className="d-flex align-items-center flex-wrap">
                                                            <span className="text-dark-75 mb-1 font-size-lg font-weight-bolder pr-6 flex-grow-1">{reply.name}</span>
                                                        </div>
                                                        <span className="text-dark-75 font-size-sm font-weight-normal border-bottom pb-4">
                                                            {reply.message}
                                                            <div className="d-flex flex-column align-items-start mt-1">
                                                                <div className="d-flex align-items-center justify-content-between">
                                                                    <span className="label label-sm label-rounded label-secondary" title={reply.flag_sent === 1 ? 'Sent' : reply.flag_sent === 2 ? 'Failed' : 'Waiting'}>
                                                                        {
                                                                            reply.flag_sent === 1 ? <i className="fas fa-check-double icon-xs m-1 text-success" />
                                                                            : reply.flag_sent === 2 ? <i className="fas fa-exclamation-circle icon-xs m-1 text-danger" />
                                                                            : <i className="fas fa-clock icon-xs m-1" />
                                                                        }
                                                                    </span>
                                                                    <label className="text-muted font-size-xs m-1">{(reply.date_create).replace('T', ' ').slice(0, 19)}</label>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                            })
                                    }
                                    {
                                        user_level === 'L1' &&
                                        <FeedFormSend
                                            message={message}
                                            setMessage={setMessage}
                                            sendReplyMessage={sendReplyMessage}
                                        />
                                    }
                                </div>
                            }
                        </div>
                    </div>
                })
            }
        </div>
    )
}

export default FeedConversation