import React, { useEffect, useState } from 'react';
import { Column, DataGrid, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import { urlAttachment } from 'app/config';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiAttachment_TicketUpload, apiAttachment_TicketList } from 'app/services/apiAttachment';

const TicketAttachment = ({ ticket_number, username }) => {
    const dispatch = useDispatch();
    const [attachment, setAttachment] = useState('');
    const [preview, setPreview] = useState('');
    const [loading, setLoading] = useState('');
    const { register, formState: { errors }, handleSubmit, reset } = useForm();
    const { attachment_ticket } = useSelector(state => state.attachment);

    useEffect(() => {
        reset({ ticket_number, created_by: username })
    }, [reset]);

    useEffect(() => {
        dispatch(apiAttachment_TicketList({ ticket_number }))
    }, [dispatch]);

    const onSubmitInsertData = async (data) => {
        setLoading('spinner spinner-white spinner-left');
        try {
            const { payload } = await dispatch(apiAttachment_TicketUpload(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data package!');
                setLoading('');
                setAttachment('');
            }
            else {
                SwalAlertError('Failed Action.', 'Please try again.');
                setLoading('');
            }
        }
        catch (error) {
            SwalAlertError('Failed Action.', `Please try again, ${error.message}.`);
            setLoading('');
        }
    }

    return (
        <div className="border rounded p-4 my-2">
            <form onSubmit={handleSubmit(onSubmitInsertData)} encType="multipart/form-data">
                <input type="hidden" {...register("ticket_number", { required: true })} />
                <input type="hidden" {...register("created_by", { required: true })} />

                <div className="row">
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label>1. Upload Attachment * :</label>
                            <div className={`custom-file ${attachment && 'hide'}`}>
                                <input type="file" {...register("attachment_file", { required: true })} className="custom-file-input" onChange={(e) => setAttachment(e.target.files[0])} />
                                <label className="custom-file-label" htmlFor="fileupload">Browse</label>
                            </div>
                            <div className={`${(!attachment) && 'hide'}`}>
                                <div className="form-control alert alert-custom alert-light-dark fade show p-2" role="alert">
                                    <div className="alert-text text-truncate" title={attachment?.name}>{attachment?.name} ({attachment?.size / 1000} KB)</div>
                                    <div className="alert-close">
                                        <button type="button" onClick={() => setAttachment('')} className="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i className="ki ki-close" title="Hapus Berkas" /></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {errors.attachment_file && <span className="form-text text-danger">* Upload Attachment File.</span>}
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label>2. Description (Optional) :</label>
                            <textarea rows="1" {...register("description", { required: false })} className="form-control form-control-md" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label>3. Upload :</label>
                            <button type="submit" className={`form-control btn btn-primary font-weight-bold btn-sm ${loading}`} disabled={loading ? true : false}>
                                <i className="fa fa-upload fa-sm" />
                                Upload
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <DataGrid
                dataSource={attachment_ticket}
                remoteOperations={{
                    filtering: true,
                    sorting: true,
                    paging: true,
                    summary: true
                }}
                allowColumnReordering={true}
                allowColumnResizing={true}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={true}
            >
                <HeaderFilter visible={true} />
                <FilterRow visible={true} />
                <Paging defaultPageSize={5} />
                <Pager
                    visible={true}
                    displayMode='full'
                    showInfo={true}
                    showNavigationButtons={true} />
                <Column caption="Actions" dataField="id" cellRender={({ data }) => {
                    return <div className="d-flex justify-content-center">
                        <a href={urlAttachment + '/ticket/' + data.file_name} target="_blank" rel="noopener noreferrer" className="fs-6 text-hover-primary fw-bold mr-2">
                            <i className="fa fa-download fa-sm" />
                        </a>
                        <a onClick={() => setPreview({ ext: data.file_type, url: urlAttachment + '/ticket/' + data.file_name })} href="#" data-toggle="modal" className="fs-6 text-hover-primary fw-bold">
                            <i className="fa fa-eye fa-sm" />
                        </a>
                    </div>
                }} />
                <Column caption="Filename" dataField="file_name" />
                <Column caption="Created By" dataField="created_by" />
                <Column caption="Created At" dataField="created_at" dataType="datetime" />
                <Column caption="Description" dataField="description" />
            </DataGrid>

            <div className="separator separator-dashed separator-border-2 my-4"></div>
            {
                (preview.ext === 'Image' || preview.ext === 'Video' || preview.ext === 'Audio') &&
                <div>
                    <h3 className="font-weight-bold">Media Preview</h3>
                    <iframe src={preview.url} width="100%" height={460} frameBorder="0"></iframe>
                </div>
            }
            {
                (preview.ext === 'Document' || preview.ext === 'Spreadsheet' || preview.ext === 'Text') &&
                <div>
                    <div>
                        <h3 classname="font-weight-bold">Document Preview</h3>
                        <iframe src={`https://view.officeapps.live.com/op/view.aspx?src=${preview.url}`} width="100%" height={460} frameBorder={0} />
                    </div>

                </div>
            }
        </div>
    )
}

export default TicketAttachment;
