import React from 'react'
import { IconGroupChat } from 'views/components/icon';
import { baseUrl } from 'app/config';

function FeedContent({ detail_feed, conversation_feeds }) {
    return (
        <div>
            <div className="bgi-no-repeat bgi-size-cover rounded min-h-295px pt-4">
                {
                    // check media type VIDEO & IMAGE
                    detail_feed.media_type === 'VIDEO' &&
                    <a href={baseUrl + '/' + detail_feed.media_url} target="_blank" rel="noopener noreferrer" title="Click to Show">
                        <video src={baseUrl + '/' + detail_feed.media_url} height={145} className="border" alt={detail_feed.post_id} />
                    </a>
                }
                {
                    (detail_feed.media_type === 'IMAGE' || detail_feed.media_type === 'CAROUSEL_ALBUM' || detail_feed.media_type === '') &&
                    <a href={baseUrl + '/' + detail_feed.media_url} target="_blank" rel="noopener noreferrer" title="Click to Show">
                        <img src={baseUrl + '/' + detail_feed.media_url} height={145} className="border" alt={detail_feed.post_id} />
                    </a>
                }
            </div>
            <p className="text-dark-75 font-size-lg font-weight-normal pt-4">
                {detail_feed.caption}
            </p>
            <div className="pt-4">
                <button type="button" className="btn btn-outline-secondary btn-sm rounded font-weight-bolder font-size-sm p-2">
                    <IconGroupChat className="svg-icon svg-icon-md pr-2" /> {[...new Set(conversation_feeds)]?.length} Comments & Replies
                </button>
            </div>
            <div className="separator separator-solid mt-4" />
        </div>
    )
}

export default FeedContent