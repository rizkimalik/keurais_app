import { createSlice } from "@reduxjs/toolkit";
import { apiSubcribedChannel, apiAssignedDepartment, apiSubcribedChannelDetail } from 'app/services/apiSubscription'

const sliceSubscription = createSlice({
    name: "subscription",
    initialState: {
        subcribed_channel: [],
        subcribed_channel_detail: {},
        assigned_department: [],
    },
    extraReducers: {
        [apiSubcribedChannel.fulfilled]: (state, action) => {
            state.subcribed_channel = action.payload.data
        },
        [apiSubcribedChannelDetail.fulfilled]: (state, action) => {
            state.subcribed_channel_detail = action.payload.data[0]
        },
        [apiAssignedDepartment.fulfilled]: (state, action) => {
            state.assigned_department = action.payload
        },
    },
});

export default sliceSubscription;