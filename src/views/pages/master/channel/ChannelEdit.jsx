import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { apiChannelList, apiChannelShow, apiChannelUpdate } from 'app/services/apiChannel'
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import { authUser } from 'app/slice/sliceAuth';
import FormInput from 'views/components/FormInput';

function ChannelEdit() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { id } = useParams();
    const { register, formState: { errors }, handleSubmit, reset } = useForm({ defaultValues: { active: 1 } });
    const { channels } = useSelector(state => state.channel)


    useEffect(() => {
        async function getChannelShow() {
            try {
                const { payload } = await dispatch(apiChannelShow({ id }))
                if (payload.status === 200) {
                    const {
                        id,
                        channel,
                        channel_type,
                        description,
                        active,
                    } = payload.data;
                    reset({
                        id,
                        channel,
                        description,
                        channel_type,
                        active,
                    });
                }
            }
            catch (error) {
                console.log(error);
            }
        }
        getChannelShow();
        dispatch(apiChannelList());
    }, [id, dispatch, reset]);

    const onSubmitEditChannel = async (data) => {
        try {
            data.updated_by = username;
            const { payload } = await dispatch(apiChannelUpdate(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Update Success', 'Success update data.');
                history.push('/channel');
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Channel Ticket" modul_name="Channel Edit" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Update Channel" subtitle="Form update channel." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitEditChannel)} className="form">
                        <CardBody>
                            <FormInput
                                name="channel"
                                type="datalist"
                                list="channel_list"
                                datalist={channels.map((item) => item.channel)}
                                label="Channel Ticket"
                                className="form-control"
                                placeholder="Enter Channel"
                                register={register}
                                rules={{ required: true, maxLength: 100 }}
                                readOnly={false}
                                errors={errors.channel}
                            />
                            <FormInput
                                name="channel_type"
                                type="text"
                                label="Channel Type"
                                className="form-control"
                                placeholder="Enter Channel Type"
                                register={register}
                                rules={{ maxLength: 100 }}
                                readOnly={false}
                                errors={errors.channel_type}
                            />
                            <FormInput
                                name="description"
                                type="textarea"
                                label="Description"
                                className="form-control"
                                placeholder="Enter Description"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.description}
                            />
                            <FormInput
                                name="active"
                                type="checkbox"
                                label="Active"
                                className="switch switch-outline switch-icon switch-sm switch-primary ml-4"
                                register={register}
                                rules=""
                                readOnly={false}
                                errors={errors.active}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/channel" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ChannelEdit