import React, { useEffect, useRef, useState } from 'react'
import * as echarts from 'echarts';
import 'echarts-wordcloud';
import HtmlEditor, { Toolbar, Item, MediaResizing } from 'devextreme-react/html-editor';
import { useDispatch } from 'react-redux';
import { apiScrollChat } from 'app/services/apiSosmed';

function General() {
    const chartDom = useRef();
    const chartWordCloud = useRef();
    const dispatch = useDispatch();
    const refConversation = useRef();
    const refContentChat = useRef();

    const [data, setData] = useState([]);
    const [page, setPage] = useState(0);
    const [loading, setLoading] = useState(false);

    
    useEffect(() => {
        const myChart = echarts.init(chartDom.current);
        const option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                feature: {
                    dataView: { show: true, readOnly: false },
                    magicType: { show: true, type: ['line', 'bar'] },
                    restore: { show: true },
                    saveAsImage: { show: true }
                }
            },
            legend: {
                data: ['Evaporation', 'Precipitation', 'Temperature']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Precipitation',
                    min: 0,
                    max: 250,
                    interval: 50,
                    axisLabel: {
                        formatter: '{value} ml'
                    }
                },
                {
                    type: 'value',
                    name: 'Temperature',
                    min: 0,
                    max: 25,
                    interval: 5,
                    axisLabel: {
                        formatter: '{value} °C'
                    }
                }
            ],
            series: [
                {
                    name: 'Evaporation',
                    type: 'bar',
                    data: [
                        2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3
                    ]
                },
                {
                    name: 'Precipitation',
                    type: 'bar',
                    data: [
                        2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3
                    ]
                },
                {
                    name: 'Temperature',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
                }
            ]
        };
        option && myChart.setOption(option);

        window.onSelectPicker();
    }, [])

    useEffect(() => {
        const text =
            'Chapter 1. Down the Rabbit-Hole ' +
            'Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do: ' +
            'once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations ' +
            'in it, \'and what is the use of a book,\' thought Alice \'without pictures or conversation?\'' +
            'So she was considering in her own mind (as well as she could, for the hot day made her feel very sleepy ' +
            'and stupid), whether the pleasure of making a daisy-chain would be worth the trouble of getting up and picking ' +
            'the daisies, when suddenly a White Rabbit with pink eyes ran close by her.';
        // Remove punctuation and convert to lowercase
        const cleanedText = text.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, "").toLowerCase();
        // Split the text into words
        const words = cleanedText.split(/\s+/);
        // Create an object to store word counts
        const commonConjunctions = ["and", "or", "but", "so", "for", "nor", "yet", "after", "although", "because", "before", "if", "since", "unless", "until", "when", "while", "of", "in", "the", "a", "to", "was", "it", "as", "by", "is", "she", "her", "had"];
        const wordCount = {};

        // Count the occurrences of each word
        for (const word of words) {
            if (!commonConjunctions.includes(word)) {
                if (word in wordCount) {
                    wordCount[word]++;
                } else {
                    wordCount[word] = 1;
                }
            }
        }

        const wordCountArray = [];
        for (const word in wordCount) {
            wordCountArray.push({ name: word, value: wordCount[word] });
        }
        console.log(wordCountArray);

        const mychartWordCloud = echarts.init(chartWordCloud.current);
        const option_wordcloud = {
            tooltip: {},
            series: [{
                type: 'wordCloud',
                gridSize: 5,
                sizeRange: [12, 50],
                // rotationRange: [-90, 90],
                shape: 'circle', // Shapes: pentagon, star, random-light, random-dark, circle, cardioid, diamond, triangle-forward, triangle, triangle-upright, apple, heart shape curve
                width: '100%',
                height: 600,
                drawOutOfBound: false,
                data: wordCountArray
            }]
        };

        option_wordcloud && mychartWordCloud.setOption(option_wordcloud);


        // return () => {
        //     // stop the renderring
        //     mychartWordCloud.stop();
        // };
    }, [])


    // useEffect(() => {
    //     const fetchData = async () => {
    //         setLoading(true);
    //         try {
    //             const { payload } = await dispatch(apiScrollChat({ skip: page, take: 10 }));
    //             setData(prevData => [...prevData, ...payload.data]);
    //         } catch (error) {
    //             console.error('Error fetching data: ', error);
    //         } finally {
    //             setLoading(false);
    //         }
    //     };
    //     fetchData();

    //     const ContentChat = document.getElementById('chat-content');
    //     setTimeout(() => {
    //         ContentChat.scrollTo({ bottom: 0, behavior: 'smooth' }, ContentChat.scrollHeight); //? auto scroll to down
    //     }, 2000);
    // }, [page]);
    

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const { payload } = await dispatch(apiScrollChat({ skip: 0, take: 10 }));
                const newdata = payload.data;
                // setData(prevData => [...prevData, ...newdata]);
                setData(prevData => [ ...newdata, ...prevData]);
                setPage(10);
            } catch (error) {
                console.error('Error fetching data: ', error);
            } finally {
                setLoading(false);
            }

        };
        fetchData();

        const ContentChat = document.getElementById('chat-content');
        setTimeout(() => {
            ContentChat.scrollTo({ bottom: 0, behavior: 'smooth' }, ContentChat.scrollHeight); //? auto scroll to down
        }, 1000);
    }, [dispatch]);

    const fetchData = async () => {
        setLoading(true);
        try {
            const { payload } = await dispatch(apiScrollChat({ skip: page, take: 10 }));
            const newdata = payload.data;
            setPage(page + 10);
            setData(prevData => [ ...newdata, ...prevData]);
        } catch (error) {
            console.error('Error fetching data: ', error);
        } finally {
            setLoading(false);
        }
    };

    const handleScroll = () => {
        const ContentChat = document.getElementById('chat-content');
        if (ContentChat.scrollTop === 0 && !loading) {
            fetchData();
        }
    }

    const scrollToTop = () => {
        const ContentChat = document.getElementById('chat-content');
        ContentChat.scrollTo({ top: 0, behavior: 'smooth' });
        handleScroll();
        // ContentChat.addEventListener('scroll', handleScroll);
        // return () => ContentChat.removeEventListener('scroll', handleScroll);
    }

    return (
        <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div className="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
                <div className="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <button type="button" onClick={() => {
                        setPage(0)
                        setData([])
                    }}>reset</button>
                    <button onClick={scrollToTop}>Scroll to Top</button>
                </div>
            </div>
            <div className="d-flex flex-column-fluid">
                <div className="container-fluid">
                    <div className="row bg-secondary">
                        <div className="col-lg-12"> {data.length} {page}
                            <div id="chat-content" ref={refContentChat} style={{ height: '480px', overflowY: 'auto' }}>
                                <div ref={refConversation} className="messages p-8">
                                    {[...new Set(data.sort((a, b) => a.id - b.id))].map((item, index) => (
                                        <div className="d-flex justify-content-start mb-10" key={index}>
                                            <div className="d-flex flex-column align-items-start">
                                                <div className="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">
                                                    {item.id} - {item.message}
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            {loading && <div>Loading...</div>}
                        </div>
                    </div>
                    
                    <div className="row">
                        <div className="col-lg-12">
                            <div style={{ height: '500px', width: '100%' }} ref={chartWordCloud} />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-6">
                            <h3>selectpicker</h3>
                            <select className="form-control selectpicker" data-size="7" data-live-search="true">
                                <option data-icon="la la-bullhorn font-size-lg bs-icon" value="AZ">Arizona</option>
                                <option value="CO">Colorado</option>
                                <option value="ID">Idaho</option>
                            </select>
                            <input type="datetime-local" className="form-control form-control-sm" />
                        </div>
                        <div className="col-lg-6">
                            <div style={{ height: '300px', width: '100%' }} ref={chartDom} />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-12">
                            <HtmlEditor
                                height={300}
                                allowPreview={true}
                            // defaultValue={valueContent}
                            // valueType={editorValueType}
                            // onValueChanged={this.valueChanged}
                            >
                                <MediaResizing enabled={true} />
                                <Toolbar multiline={true}>
                                    <Item name="undo" />
                                    <Item name="redo" />
                                    <Item name="separator" />
                                    <Item
                                        name="size"
                                        acceptedValues={['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt']}
                                    />
                                    <Item
                                        name="font"
                                        acceptedValues={['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana']}
                                    />
                                    <Item name="separator" />
                                    <Item name="bold" />
                                    <Item name="italic" />
                                    <Item name="strike" />
                                    <Item name="underline" />
                                    <Item name="separator" />
                                    <Item name="alignLeft" />
                                    <Item name="alignCenter" />
                                    <Item name="alignRight" />
                                    <Item name="alignJustify" />
                                    <Item name="separator" />
                                    <Item name="orderedList" />
                                    <Item name="bulletList" />
                                    <Item name="separator" />
                                    <Item name="color" />
                                    <Item name="background" />
                                    <Item name="separator" />
                                    <Item name="link" />
                                    <Item name="image" />
                                    <Item name="separator" />
                                    <Item name="separator" />
                                    <Item name="clear" />
                                    <Item name="codeBlock" />
                                    <Item name="blockquote" />
                                    <Item name="separator" />
                                    <Item name="insertTable" />
                                    <Item name="deleteTable" />
                                    <Item name="insertRowAbove" />
                                    <Item name="insertRowBelow" />
                                    <Item name="deleteRow" />
                                    <Item name="insertColumnLeft" />
                                    <Item name="insertColumnRight" />
                                    <Item name="deleteColumn" />
                                </Toolbar>
                            </HtmlEditor>

                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            {/* <DropDownBox
                                // value="1"
                                valueExpr="ID"
                                deferRendering={false}
                                displayExpr="City"
                                placeholder="Select a value..."
                                // showClearButton={true}
                                // dataSource={databox}
                                // onValueChanged={this.syncDataGridSelection}
                                contentRender={() => {
                                    return <TicketReference />
                                }}
                            /> */}


                            {/* <DataGrid
                            dataSource={databox}
                            height={345}
                            hoverStateEnabled={true}
                            selectedRowKeys=""
                            onSelectionChanged=""
                            allowColumnReordering={true}
                            allowColumnResizing={true}
                            columnAutoWidth={true}
                            showBorders={true}
                            showColumnLines={true}
                            showRowLines={true}
                        > */}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default General
