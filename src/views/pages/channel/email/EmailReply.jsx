import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import HtmlEditor, { Toolbar, Item, MediaResizing, TableResizing } from 'devextreme-react/html-editor';

import FormInput from 'views/components/FormInput';
import FormGroup from 'views/components/FormGroup';
import SplashScreen from 'views/components/SplashScreen';
import { authUser } from 'app/slice/sliceAuth';
import { IconMailOpen } from 'views/components/icon';
import { Modal, ModalBody, ModalFooter } from 'views/components/modal'
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';
import { apiEmailAccount, apiInsertEmailOut, apiEmailDetail } from 'app/services/apiEmail';

function EmailReply({ email_detail }) {
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const { email_account } = useSelector(state => state.email);
    const [email_data_detail, setEmailDetail] = useState('');

    useEffect(() => {
        dispatch(apiEmailAccount({ action: 'outbound' }));
    }, [dispatch]);

    useEffect(async () => {
        setEmailDetail('');
        const { payload } = await dispatch(apiEmailDetail({ email_id: email_detail.email_id }));
        if (payload.status === 200) {
            setEmailDetail(payload.data);
        }
        else {
            setEmailDetail('');
        }
    }, [dispatch, email_detail]);

    useEffect(() => {
        const interaction_id = `[#MDW-${email_detail.interaction_id}] `; //? check ticket
        let subject = '';

        if (Boolean((email_detail.esubject).indexOf('[#MDW-') + 1) && Boolean((email_detail.esubject).indexOf(']'))) {
            subject = email_detail.esubject;
        }
        else {
            subject = `${interaction_id} RE: ${email_detail.esubject}`;
        }

        reset({
            eto: email_detail.efrom,
            ecc: email_detail.ecc,
            esubject: subject
        });
    }, [reset, email_detail]);

    const onSubmitFormEmail = async (data, e) => {
        e.preventDefault();

        try {
            // data.ebody_html = document.querySelector('input[name=ebody_html]').value + '<hr />' + email_data_detail.ebody_html;
            data.ebody_html = document.querySelector('input[name=ebody_html]').value;
            data.agent_handle = username;
            data.reply_id = email_detail.email_id;
            data.interaction_id = email_detail.interaction_id;
            data.ticket_number = email_detail.ticket_number;
            data.status = 'EmailReply';

            const { payload } = await dispatch(apiInsertEmailOut(data));
            if (payload.status === 200) {
                reset();
                document.querySelector('input[name=ebody_html]').value = '';
                SwalAlertSuccess('Reply Success', 'reply email out success.')
            }
            else {
                SwalAlertError('Submit Failed', 'Please try again.')
            }
        }
        catch (error) {
            SwalAlertError('Submit Failed', error.message)
        }
    }

    return (
        <Modal id="modalEmailReply" modal_size="modal-lg">
            {/* <ModalHeader title='Form Email' /> */}
            {/* <form onSubmit={handleSubmit(onSubmitFormEmail)} id="form-reply-email" name="form-reply-email"> */}
            <form id="form-reply-email" name="form-reply-email">
                <ModalBody className="p-0">
                    {!email_data_detail && <SplashScreen />}
                    <div className="d-block px-5 pt-5">
                        <FormGroup label="From">
                            <select name="efrom"
                                {...register("efrom", { required: true })}
                                className="form-control form-control-sm">
                                <option value="">-- select email --</option>
                                {
                                    email_account?.map((item, index) => {
                                        return <option value={item.username} key={index}>{item.username}</option>
                                    })
                                }
                            </select>
                            {errors.efrom && <span className="form-text text-danger">Please select email.</span>}
                        </FormGroup>
                        <FormInput
                            name="eto"
                            type="text"
                            label="To"
                            className="form-control form-control-sm"
                            placeholder="Enter Email"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.eto}
                        />
                        <FormInput
                            name="ecc"
                            type="text"
                            label="CC"
                            className="form-control form-control-sm"
                            placeholder="Enter CC"
                            register={register}
                            rules={{ required: false }}
                            readOnly={false}
                            errors={errors.ecc}
                        />
                        <FormInput
                            name="esubject"
                            type="text"
                            label="Subject"
                            className="form-control form-control-sm"
                            placeholder="Enter Subject"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.esubject}
                        />
                        <HtmlEditor
                            className="my-2"
                            height={350}
                            valueType="html"
                            onValueChange={(value) => document.querySelector('input[name=ebody_html]').value = value}
                        >
                            <MediaResizing enabled={true} />
                            <TableResizing enabled={true} />
                            <Toolbar multiline={true}>
                                <Item name="undo" />
                                <Item name="redo" />
                                <Item name="separator" />
                                <Item
                                    name="size"
                                    acceptedValues={['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt']}
                                />
                                <Item
                                    name="font"
                                    acceptedValues={['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana']}
                                />
                                <Item name="separator" />
                                <Item name="bold" />
                                <Item name="italic" />
                                <Item name="strike" />
                                <Item name="underline" />
                                <Item name="separator" />
                                <Item name="alignLeft" />
                                <Item name="alignCenter" />
                                <Item name="alignRight" />
                                <Item name="alignJustify" />
                                <Item name="separator" />
                                <Item name="orderedList" />
                                <Item name="bulletList" />
                                <Item name="separator" />
                                <Item name="color" />
                                <Item name="background" />
                                <Item name="separator" />
                                <Item name="link" />
                                {/* <Item name="image" /> */}
                                <Item name="separator" />
                                <Item name="clear" />
                                <Item name="codeBlock" />
                                <Item name="blockquote" />
                                <Item name="separator" />
                                <Item name="insertTable" />
                                <Item name="deleteTable" />
                                <Item name="insertRowAbove" />
                                <Item name="insertRowBelow" />
                                <Item name="deleteRow" />
                                <Item name="insertColumnLeft" />
                                <Item name="insertColumnRight" />
                                <Item name="deleteColumn" />
                            </Toolbar>
                        </HtmlEditor>
                        <input type="hidden" className="form-control" name="ebody_html" title="tampung data body email" />
                        {/* {document.querySelector('input[name=ebody_html]').value === '' && <span className="form-text text-danger">*Please Enter email body</span>} */}
                    </div>
                    <div className="accordion accordion-toggle-arrow px-5 pb-2" id="accordionEmailDetail">
                        <div className="card">
                            <div className="card-header">
                                <div className="card-title collapsed" data-toggle="collapse" data-target="#collapseEmailDetail" aria-expanded="false">
                                    <IconMailOpen className="svg-icon svg-icon-xs" /> Email Detail
                                </div>
                            </div>
                            <div id="collapseEmailDetail" className="collapse" data-parent="#accordionEmailDetail">
                                <div className="card-body">
                                    <div className="p-5" style={{ height: '400px', overflow: 'auto' }}>
                                        <div dangerouslySetInnerHTML={{ __html: email_data_detail.ebody_html }}></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button type="button" onClick={handleSubmit(onSubmitFormEmail)} name="btn-reply-email" id="btn-reply-email" className="btn btn-primary font-weight-bold px-6">Send</button>
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default EmailReply