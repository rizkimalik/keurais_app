import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons'
import FormInput from 'views/components/FormInput';
import FormGroup from 'views/components/FormGroup';
import ModalFilterByCustomer from './ModalFilterByCustomer';
import { SwalAlertError } from 'views/components/SwalAlert';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiReportSLA, apiReportSLAExport } from 'app/services/apiReport';

function ReportSLA() {
    const dispatch = useDispatch();
    const { report_sla } = useSelector(state => state.report);
    const [navigate, setNavigate] = useState('');
    const [selected_customer, onSelectedCustomer] = useState('');
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();

    useEffect(() => {
        let today = new Date();
        const date_start = new Date(new Date().setDate(new Date().getDate() - 30)).toISOString().slice(0, 10);
        const date_end = today.toISOString().slice(0, 10);
        setValue('date_start', date_start);
        setValue('date_end', date_end);
    }, [setValue]);

    useEffect(() => {
        setValue('customer_id', selected_customer.customer_id)
        setValue('customer_name', selected_customer.name)
    }, [setValue, selected_customer]);

    const onSubmitReportSLA = async (data) => {
        try {
            const result = await dispatch(apiReportSLA(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value,
                customer_id: document.querySelector('input[name="customer_id"]').value
            }
            const result = await dispatch(apiReportSLAExport(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');
                worksheet.columns = [
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Status', key: 'status' },
                    { header: 'SLA (Days)', key: 'sla' },
                    { header: 'SLA Total (Days)', key: 'sla_total' },
                    { header: 'SLA % (Percentage)', key: 'sla_percentage' },
                    { header: 'SLA L1 (Days)', key: 'L1_diff_day' },
                    { header: 'SLA L2 (Days)', key: 'L2_diff_day' },
                    { header: 'SLA L3 (Days)', key: 'L3_diff_day' },
                    { header: 'Customer ID', key: 'customer_id' },
                    { header: 'Customer Name', key: 'customer_name' },
                    { header: 'Channel', key: 'ticket_source' },
                    { header: 'Channel Type', key: 'source_information' },
                    { header: 'Ticket Position', key: 'dispatch_to_layer' },
                    { header: 'Category', key: 'category_name' },
                    { header: 'Category Product', key: 'category_sublv1_name' },
                    { header: 'Category Case', key: 'category_sublv2_name' },
                    { header: 'Category Detail', key: 'category_sublv3_name' },
                    { header: 'Complaint Detail', key: 'complaint_detail' },
                    { header: 'Response Detail', key: 'response_detail' },
                    { header: 'User Created', key: 'user_create' },
                    { header: 'Date Created', key: 'date_created' },
                    { header: 'User Closed', key: 'user_closed' },
                    { header: 'Date Closed', key: 'date_closed' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:W1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Report_BaseOnSLA_${data.date_start}_${data.date_end}.xlsx`);
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Base on SLA" />
            <Container>
                {navigate === 'ModalFilterByCustomer' && <ModalFilterByCustomer onSelectedCustomer={onSelectedCustomer} />}
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Base on SLA</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportSLA)} id="formReportSLA">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-3">
                                <FormGroup label="Customer">
                                    <div className="input-group">
                                        <input type="text"
                                            {...register("customer_id", { required: false })}
                                            name="customer_id"
                                            className="form-control form-control-sm hide"
                                        />
                                        <input type="text"
                                            {...register("customer_name", { required: false })}
                                            name="customer_name"
                                            className="form-control form-control-sm"
                                            placeholder="Customer..."
                                            readOnly={true}
                                        />
                                        <div className="input-group-append">
                                            <button type="button" onClick={() => setNavigate('ModalFilterByCustomer')} className="btn btn-light-primary btn-sm" data-toggle="modal" data-target="#ModalFilterByCustomer">
                                                <Icons iconName="search" className="svg-icon svg-icon-sm" />
                                            </button>
                                        </div>
                                    </div>
                                </FormGroup>
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_sla}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <GroupPanel visible={true} />
                    <Column caption="Ticket Number" dataField="ticket_number" />
                    <Column caption="Status" dataField="status" />
                    <Column caption="SLA (Days)" dataField="sla" />
                    <Column caption="SLA Total (Days)" dataField="sla_total" />
                    <Column caption="SLA % (Percentage)" dataField="sla_percentage" cellRender={(data) => {
                        return data.value < 100 ? <span className="text-danger">{(data.value).toFixed(2)} % </span> : <span className="text-success">{(data.value).toFixed(2)} % </span>;
                    }} />
                    <Column caption="SLA L1 (Days)" dataField="L1_diff_day" />
                    <Column caption="SLA L2 (Days)" dataField="L2_diff_day" />
                    <Column caption="SLA L3 (Days)" dataField="L3_diff_day" />
                    <Column caption="CustomerID" dataField="customer_id" />
                    <Column caption="Customer Name" dataField="customer_name" />
                    <Column caption="Channel" dataField="ticket_source" />
                    <Column caption="Channel Type" dataField="source_information" />
                    <Column caption="Ticket Position" dataField="dispatch_to_layer" />
                    <Column caption="Category" dataField="category_name" />
                    <Column caption="Category Product" dataField="category_sublv1_name" />
                    <Column caption="Category Case" dataField="category_sublv2_name" />
                    <Column caption="Category Detail" dataField="category_sublv3_name" />
                    <Column caption="Complaint Detail" dataField="complaint_detail" />
                    <Column caption="Response Detail" dataField="response_detail" />
                    <Column caption="User Created" dataField="user_create" />
                    <Column caption="Date Created" dataField="date_created" />
                    <Column caption="User Closed" dataField="user_closed" />
                    <Column caption="Date Closed" dataField="date_closed" />
                </DataGrid>

            </Container>
        </MainContent>
    )
}

export default ReportSLA