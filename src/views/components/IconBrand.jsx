import React from 'react'

function IconBrand({ name, height, width }) {
    if (name === 'Chat') {
        return <div><img src="./assets/media/icons/chat.png" height={height} width={width} alt="chat" /></div>;
    } else if (name === 'Email') {
        return <div><img src="./assets/media/icons/email.png" height={height} width={width} alt="email" /></div>;
    } else if (name === 'Messenger') {
        return <div><img src="./assets/media/icons/messenger.png" height={height} width={width} alt="messenger" /></div>;
    } else if (name === 'Twitter') {
        return <div><img src="./assets/media/icons/twitter.png" height={height} width={width} alt="twitter" /></div>;
    } else if (name === 'Facebook') {
        return <div><img src="./assets/media/icons/facebook.png" height={height} width={width} alt="facebook" /></div>;
    } else if (name === 'Whatsapp') {
        return <div><img src="./assets/media/icons/whatsapp.png" height={height} width={width} alt="whatsapp" /></div>;
    } else if (name === 'Instagram') {
        return <div><img src="./assets/media/icons/instagram.png" height={height} width={width} alt="instagram" /></div>;
    } else if (name === 'Call') {
        return <div><img src="./assets/media/icons/call.png" height={height} width={width} alt="call" /></div>;
    } else if (name === 'Telegram') {
        return <div><img src="./assets/media/icons/telegram.png" height={height} width={width} alt="telegram" /></div>;
    } else if (name === 'All') {
        return <div><img src="./assets/media/icons/omnichannel.png" height={height} width={width} alt="omnichannel" /></div>;
    }
    else {
        return '';
    }
}

export default IconBrand