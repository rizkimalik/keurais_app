import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import CustomStore from "devextreme/data/custom_store";

export const apiSubcribedChannel = createAsyncThunk(
    "subscription/apiSubcribedChannel",
    async () => {
        const res = await axios.post('/subscription/data_subscription_channel').catch((error) => { return error.response });
        return res.data;
    }
)

export const apiSubcribedChannelDetail = createAsyncThunk(
    "subscription/apiSubcribedChannelDetail",
    async (param) => {
        const res = await axios.post('/subscription/detail_subscription_channel', JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiSubcribedChannelInsert = createAsyncThunk(
    "subscription/apiSubcribedChannelInsert",
    async (param) => {
        const res = await axios.post(`/subscription/insert_subscription_channel`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiSubcribedChannelUpdate = createAsyncThunk(
    "subscription/apiSubcribedChannelUpdate",
    async (param) => {
        const res = await axios.post(`/subscription/update_subscription_channel`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiSubcribedChannelDelete = createAsyncThunk(
    "subscription/apiSubcribedChannelDelete",
    async (param) => {
        const res = await axios.post(`/subscription/delete_subscription_channel`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiAssignedDepartment = createAsyncThunk(
    "subscription/apiAssignedDepartment",
    async (param) => {
        const store = new CustomStore({
            key: 'id',
            load: async (options) => {
                let params = {
                    ...param,
                    ...options
                }

                return await axios.post(`/subscription/data_assigned_department`, JSON.stringify(params))
                    .then(({ data }) => ({
                        data: data.data,
                        totalCount: data.total,
                    }))
                    .catch((error) => { return error.response });
            },
        });

        return store;
    }
)

export const apiAssignedDepartmentInsert = createAsyncThunk(
    "subscription/apiAssignedDepartmentInsert",
    async (param) => {
        const res = await axios.post(`/subscription/insert_assign_department`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiAssignedDepartmentDelete = createAsyncThunk(
    "subscription/apiAssignedDepartmentDelete",
    async (param) => {
        const res = await axios.post(`/subscription/delete_assigned_department`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiWhatsappQRCode = createAsyncThunk(
    "subscription/apiWhatsappQRCode",
    async (param) => {
        const res = await axios.post(`/subscription/whatsapp_scan_qrcode`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)

export const apiSeleniumAutoLogin = createAsyncThunk(
    "subscription/apiSeleniumAutoLogin",
    async (param) => {
        const res = await axios.post(`/subscription/selenium_autologin`, JSON.stringify(param)).catch((error) => { return error.response });
        return res.data;
    }
)