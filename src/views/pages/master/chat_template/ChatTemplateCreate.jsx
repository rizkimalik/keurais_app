import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { SubHeader, MainContent, Container } from 'views/layouts/partials';
import { Card, CardBody, CardFooter, CardHeader, CardTitle } from 'views/components/card';
import { ButtonCancel, ButtonSubmit } from 'views/components/button';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertError, SwalAlertSuccess } from 'views/components/SwalAlert';
import FormInput from 'views/components/FormInput';
import { apiChatTemplateStore } from 'app/services/apiChatTemplate';

function ChatTemplateCreate() {
    const history = useHistory();
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { register, formState: { errors }, handleSubmit } = useForm();

    const onSubmitCreateChatTemplate = async (data) => {
        try {
            data.created_by = username;
            const { payload } = await dispatch(apiChatTemplateStore(data))
            if (payload.status === 200) {
                SwalAlertSuccess('Insert Success', 'Success into application.');
                history.push('/master/chat_template')
            }
        }
        catch (error) {
            console.log(error);
            SwalAlertError('Update Failed', 'Please try again!')
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Master Data" menu_name="Chat Template" modul_name="Chat Template Create" />
            <Container>
                <Card>
                    <CardHeader>
                        <CardTitle title="Form Chat Template" subtitle="Form add new Chat Template." />
                    </CardHeader>
                    <form onSubmit={handleSubmit(onSubmitCreateChatTemplate)} className="form">
                        <CardBody>
                            <FormInput
                                name="short_code"
                                type="text"
                                label="Title"
                                className="form-control"
                                placeholder="Enter Title"
                                register={register}
                                rules={{ required: true, maxLength: 150 }}
                                readOnly={false}
                                errors={errors.short_code}
                            />
                            <FormInput
                                name="content_message"
                                type="textarea"
                                label="Content Template"
                                className="form-control"
                                placeholder="Enter Content Template"
                                register={register}
                                rules={{ required: true}}
                                readOnly={false}
                                errors={errors.content_message}
                            />
                        </CardBody>
                        <CardFooter>
                            <ButtonCancel to="/master/chat_template" />
                            <ButtonSubmit />
                        </CardFooter>
                    </form>
                </Card>
            </Container>
        </MainContent>
    )
}

export default ChatTemplateCreate