function RandomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export default RandomString

export const RandomNumber = () => {
    const date = new Date(),
        Y = addZero(date.getFullYear()),
        M = addZero(date.getMonth()),
        D = addZero(date.getDate()),
        h = addZero(date.getHours()),
        m = addZero(date.getMinutes()),
        s = addZero(date.getSeconds());

    return `${Y}${M}${D}${h}${m}${s}`;
}

function addZero(x) {
    if (x < 10) {
        x = "0" + x;
    }
    return x;
}