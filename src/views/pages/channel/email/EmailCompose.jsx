import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import HtmlEditor, { Toolbar, Item, MediaResizing, TableResizing } from 'devextreme-react/html-editor';
import { Modal, ModalBody, ModalFooter } from 'views/components/modal'
import FormInput from 'views/components/FormInput';
import FormGroup from 'views/components/FormGroup';
import { apiEmailAccount, apiInsertEmailOut } from 'app/services/apiEmail';
import { authUser } from 'app/slice/sliceAuth';
import { SwalAlertSuccess, SwalAlertError } from 'views/components/SwalAlert';

function EmailCompose() {
    const dispatch = useDispatch();
    const { username } = useSelector(authUser);
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const { email_account } = useSelector(state => state.email);

    useEffect(() => {
        dispatch(apiEmailAccount({ action: 'outbound' }))
    }, [dispatch]);

    const onSubmitFormEmail = async (data) => {
        try {
            data.ebody_html = document.querySelector('input[name=ebody_html]').value;
            data.agent_handle = username;
            data.status = 'Compose';

            const { payload } = await dispatch(apiInsertEmailOut(data));
            if (payload.status === 200) {
                reset();
                document.querySelector('input[name=ebody_html]').value = '';
                SwalAlertSuccess('Compose Success', 'compose email out success.')
            }
            else {
                SwalAlertError('Submit Failed', 'Please try again.')
            }
        }
        catch (error) {
            SwalAlertError('Submit Failed', error.message)
        }
    }

    return (
        <Modal id="modalEmailCompose" modal_size="modal-lg">
            {/* <ModalHeader title='Compose' /> */}
            <form onSubmit={handleSubmit(onSubmitFormEmail)}>
                <ModalBody>
                    <div className="d-block">
                        {/* <input type="email" name="ecc" {...register("ecc", { required: false })} className="hide" /> */}
                        <FormGroup label="From">
                            <select name="efrom"
                                {...register("efrom", { required: true })}
                                className="form-control form-control-sm"
                            >
                                <option value="">-- select email --</option>
                                {
                                    email_account?.map((item, index) => {
                                        return <option value={item.username} key={index}>{item.username}</option>
                                    })
                                }
                            </select>
                            {errors.efrom && <span className="form-text text-danger">Please select email.</span>}
                        </FormGroup>
                        <FormInput
                            name="eto"
                            type="text"
                            label="To"
                            className="form-control form-control-sm"
                            placeholder="Enter Email"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.eto}
                        />
                        <FormInput
                            name="ecc"
                            type="text"
                            label="CC"
                            className="form-control form-control-sm"
                            placeholder="Enter CC"
                            register={register}
                            rules={{ required: false }}
                            readOnly={false}
                            errors={errors.ecc}
                        />
                        <FormInput
                            name="esubject"
                            type="text"
                            label="Subject"
                            className="form-control form-control-sm"
                            placeholder="Enter Subject"
                            register={register}
                            rules={{ required: true }}
                            readOnly={false}
                            errors={errors.esubject}
                        />
                        <HtmlEditor
                            className="my-2"
                            height={350}
                            valueType="html"
                            onValueChange={(value) => document.querySelector('input[name=ebody_html]').value = value}
                        >
                            <MediaResizing enabled={true} />
                            <TableResizing enabled={true} />
                            <Toolbar multiline={true}>
                                <Item name="undo" />
                                <Item name="redo" />
                                <Item name="separator" />
                                <Item
                                    name="size"
                                    acceptedValues={['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt']}
                                />
                                <Item
                                    name="font"
                                    acceptedValues={['Arial', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma', 'Times New Roman', 'Verdana']}
                                />
                                <Item name="separator" />
                                <Item name="bold" />
                                <Item name="italic" />
                                <Item name="strike" />
                                <Item name="underline" />
                                <Item name="separator" />
                                <Item name="alignLeft" />
                                <Item name="alignCenter" />
                                <Item name="alignRight" />
                                <Item name="alignJustify" />
                                <Item name="separator" />
                                <Item name="orderedList" />
                                <Item name="bulletList" />
                                <Item name="separator" />
                                <Item name="color" />
                                <Item name="background" />
                                <Item name="separator" />
                                <Item name="link" />
                                {/* <Item name="image" /> */}
                                <Item name="separator" />
                                <Item name="clear" />
                                <Item name="codeBlock" />
                                <Item name="blockquote" />
                                <Item name="separator" />
                                <Item name="insertTable" />
                                <Item name="deleteTable" />
                                <Item name="insertRowAbove" />
                                <Item name="insertRowBelow" />
                                <Item name="deleteRow" />
                                <Item name="insertColumnLeft" />
                                <Item name="insertColumnRight" />
                                <Item name="deleteColumn" />
                            </Toolbar>
                        </HtmlEditor>
                        <input type="hidden" className="form-control" name="ebody_html" title="tampung data body email" />
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button type="submit" className="btn btn-primary font-weight-bold px-6">Send</button>
                    {/* <button type="button" className="btn btn-icon btn-sm btn-clean">
                        <i className="flaticon2-clip-symbol" />
                    </button> */}
                </ModalFooter>
            </form>
        </Modal>
    )
}

export default EmailCompose