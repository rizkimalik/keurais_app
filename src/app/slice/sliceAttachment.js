import { createSlice } from "@reduxjs/toolkit";
import {
    apiAttachment_TicketList,
} from "app/services/apiAttachment";

const sliceAttachment = createSlice({
    name: "attachment",
    initialState: {
        attachment_ticket: [],
    },
    extraReducers: {
        [apiAttachment_TicketList.fulfilled]: (state, action) => {
            state.attachment_ticket = action.payload
        },
    },
});

export default sliceAttachment;