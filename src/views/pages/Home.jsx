import React from 'react'
import { useSelector } from 'react-redux'

import { Card, CardBody } from 'views/components/card'
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { NavLink } from 'react-router-dom'
import { IconCall, IconMailHistory, IconGroupChat } from 'views/components/icon'
import { authUser } from 'app/slice/sliceAuth'

const Home = () => {
    const user = useSelector(authUser);

    return (
        <MainContent>
            <SubHeader active_page="Home" menu_name="Welcome" modul_name="Application">
                {
                    (user.user_level === 'Admin' || user.user_level === 'L1') &&
                    <div>
                        <NavLink to="/omnichannel/call" className="btn btn-light font-weight-bolder btn-sm m-1">
                            <IconCall className="svg-icon svg-icon-sm" /> Data Call
                        </NavLink>
                        <NavLink to="/omnichannel/email" className="btn btn-light font-weight-bolder btn-sm m-1">
                            <IconMailHistory className="svg-icon svg-icon-sm" /> Data Mailbox
                        </NavLink>
                        <NavLink to="/omnichannel/history" className="btn btn-light font-weight-bolder btn-sm m-1">
                            <IconGroupChat className="svg-icon svg-icon-sm" /> Data Sosial Media
                        </NavLink>
                    </div>
                }
            </SubHeader>
            <Container>
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                        <Card>
                            <CardBody className="d-flex align-items-center">
                                <div className="text-center">
                                    <p className="display-4 text-primary">Keurais Case Management</p>
                                    <p>Contact center solutions allow your contact center to connect with customers on any communication channel like voice call, video call, live chat, social media, and more. Provide customer service on any channel and seamlessly switch among any digital channels during an interaction, while maintaining context and relevant information across all channels as if it were a single conversation.</p>
                                    <img loading="lazy" src="./assets/media/ticketing.png" alt="Logo-Ticketing" style={{ width: '600px', height: 'auto' }} />
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default Home
