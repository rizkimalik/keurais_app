import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import Icons from 'views/components/Icons'
import FormInput from 'views/components/FormInput';
import FormGroup from 'views/components/FormGroup';
import ModalFilterByCustomer from './ModalFilterByCustomer';
import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { SwalAlertError } from 'views/components/SwalAlert';
import { apiReportByAgent, apiReportByAgent_Export } from 'app/services/apiReport';
import { authUser } from 'app/slice/sliceAuth';

function ReportByAgent() {
    const dispatch = useDispatch();
    const { report_by_agent } = useSelector(state => state.report);
    const [navigate, setNavigate] = useState('');
    const [selected_customer, onSelectedCustomer] = useState('');
    const { register, formState: { errors }, handleSubmit, setValue } = useForm();
    const user = useSelector(authUser);

    useEffect(() => {
        let today = new Date();
        const date_start = new Date(new Date().setDate(new Date().getDate() - 30)).toISOString().slice(0, 10);
        const date_end = today.toISOString().slice(0, 10);
        setValue('date_start', date_start);
        setValue('date_end', date_end);
    }, [setValue]);

    useEffect(() => {
        setValue('customer_id', selected_customer.customer_id)
        setValue('customer_name', selected_customer.name)
    }, [setValue, selected_customer]);

    const onSubmitReportByAgent = async (data) => {
        try {
            data.agent_handle = user.username;
            data.agent_layer = user.user_level;
            const result = await dispatch(apiReportByAgent(data));
            if (result.meta.requestStatus === 'fulfilled') {
                console.log('success data fulfilled.')
            }
            else {
                SwalAlertError('Failed Data.', 'Please Try again.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Data.', 'Please Try again.');
        }
    }

    const handlerExportExcel = async (e) => {
        e.preventDefault();

        try {
            const data = {
                date_start: document.querySelector('input[name="date_start"]').value,
                date_end: document.querySelector('input[name="date_end"]').value,
                customer_id: document.querySelector('input[name="customer_id"]').value
            }
            const result = await dispatch(apiReportByAgent_Export(data));
            if (result.payload.status === 204) return SwalAlertError('Failed Export.', result.payload.data);
            const data_export = result.payload.data;

            if (data_export.length > 0) {
                const workbook = new Workbook();
                const worksheet = workbook.addWorksheet('Main sheet');

                worksheet.columns = [
                    { header: 'User Created', key: 'user_create' },
                    { header: 'Ticket Number', key: 'ticket_number' },
                    { header: 'Interaction ID', key: 'thread_id' },
                    { header: 'CustomerID', key: 'customer_id' },
                    { header: 'Customer Name', key: 'name' },
                    { header: 'Channel', key: 'ticket_source' },
                    { header: 'Channel Type', key: 'source_information' },
                    { header: 'Status', key: 'status' },
                    { header: 'SLA (Days)', key: 'sla' },
                    { header: 'Ticket Position', key: 'ticket_position' },
                    { header: 'Date Created', key: 'date_create' },
                    { header: 'Category', key: 'category_name' },
                    { header: 'Category Product', key: 'category_sublv1_name' },
                    { header: 'Category Case', key: 'category_sublv2_name' },
                    { header: 'Category Detail', key: 'category_sublv3_name' },
                    { header: 'Complaint Detail', key: 'complaint_detail' },
                    { header: 'Response Detail', key: 'response_detail' },
                ]
                worksheet.addRows(data_export);
                worksheet.autoFilter = 'A1:N1';
                worksheet.eachRow(function (row, rowNumber) {
                    row.eachCell((cell, colNumber) => {
                        if (rowNumber === 1) {
                            cell.fill = {
                                type: 'pattern',
                                pattern: 'solid',
                                fgColor: { argb: 'f5b914' }
                            }
                        }
                    })
                    row.commit();
                });

                workbook.xlsx.writeBuffer().then((buffer) => {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Report_ByAgent_${data.date_start}_${data.date_end}.xlsx`);
                });
            }
            else {
                SwalAlertError('Failed Export.', 'Result data is empty.');
            }
        } catch (error) {
            console.log(error);
            SwalAlertError('Failed Export.', error.message);
        }
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Agent Ticket" />
            <Container>
                {navigate === 'ModalFilterByCustomer' && <ModalFilterByCustomer onSelectedCustomer={onSelectedCustomer} />}

                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between mb-4">
                        <h4 className="font-weight-bold">Report Ticket by Agent</h4>
                    </div>
                    <form onSubmit={handleSubmit(onSubmitReportByAgent)} id="formReportByAgent">
                        <div className="row">
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_start"
                                    type="date"
                                    label="Start Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_start}
                                />
                            </div>
                            <div className="col-lg-2">
                                <FormInput
                                    name="date_end"
                                    type="date"
                                    label="End Date"
                                    className="form-control form-control-sm"
                                    placeholder="Please select date"
                                    register={register}
                                    rules={{ required: true }}
                                    readOnly={false}
                                    errors={errors.date_end}
                                />
                            </div>
                            <div className="col-lg-3">
                                <FormGroup label="Customer">
                                    <div className="input-group">
                                        <input type="text"
                                            {...register("customer_id", { required: false })}
                                            name="customer_id"
                                            className="form-control form-control-sm hide"
                                        />
                                        <input type="text"
                                            {...register("customer_name", { required: false })}
                                            name="customer_name"
                                            className="form-control form-control-sm"
                                            placeholder="Customer..."
                                            readOnly={true}
                                        />
                                        <div className="input-group-append">
                                            <button type="button" onClick={() => setNavigate('ModalFilterByCustomer')} className="btn btn-light-primary btn-sm" data-toggle="modal" data-target="#ModalFilterByCustomer">
                                                <Icons iconName="search" className="svg-icon svg-icon-sm" />
                                            </button>
                                        </div>
                                    </div>
                                </FormGroup>
                            </div>
                            <div className="col-lg-4 mt-6">
                                <button className="btn btn-sm btn-primary font-weight-bolder mr-4">
                                    <Icons iconName="view" className="svg-icon" /> View
                                </button>
                                <button onClick={(e) => handlerExportExcel(e)} className="btn btn-light-primary font-weight-bolder btn-sm m-1">
                                    <Icons iconName="download" className="svg-icon svg-icon-sm" />
                                    Export
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <DataGrid
                    dataSource={report_by_agent}
                    remoteOperations={{
                        filtering: true,
                        sorting: true,
                        paging: true
                    }}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <GroupPanel visible={true} />
                    <Column caption="User Created" dataField="user_create" />
                    <Column caption="Ticket Number" dataField="ticket_number" />
                    <Column caption="Interaction ID" dataField="thread_id" />
                    <Column caption="CustomerID" dataField="customer_id" />
                    <Column caption="Customer Name" dataField="name" />
                    <Column caption="Channel" dataField="ticket_source" />
                    <Column caption="Channel Type" dataField="source_information" />
                    <Column caption="Status" dataField="status" />
                    <Column caption="SLA (Days)" dataField="sla" />
                    <Column caption="Ticket Position" dataField="ticket_position" />
                    <Column caption="Date Created" dataField="date_create" />
                    <Column caption="Category" dataField="category_name" />
                    <Column caption="Category Product" dataField="category_sublv1_name" />
                    <Column caption="Category Case" dataField="category_sublv2_name" />
                    <Column caption="Category Detail" dataField="category_sublv3_name" />
                    <Column caption="Complaint Detail" dataField="complaint_detail" />
                    <Column caption="Response Detail" dataField="response_detail" />
                </DataGrid>
            </Container>
        </MainContent>
    )
}

export default ReportByAgent