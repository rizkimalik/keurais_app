import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Button, Column, DataGrid, Editing, FilterRow, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid';
import { ButtonRefresh } from 'views/components/button';
import { Card, CardBody, CardToolbar, CardHeader, CardTitle } from 'views/components/card';
import { apiAssignSLA_ByCustomer } from 'app/services/apiCategory';
import AssignModal from './AssignModal';
import Icons from 'views/components/Icons';

function AssignedCustomerSLA({ category_sublv3_id }) {
    const dispatch = useDispatch();
    const { assigned_customer_sla } = useSelector(state => state.category);
    const [show_modal, setShowModal] = useState(false);

    useEffect(() => {
        dispatch(apiAssignSLA_ByCustomer({ category_sublv3_id }));
    }, [dispatch, category_sublv3_id]);

    return (
        <Card>
            <CardHeader>
                <CardTitle title="Custom SLA by Customer" subtitle="Assign Detail spesific customer SLA." />
                <CardToolbar>
                    <ButtonRefresh onClick={() => dispatch(apiAssignSLA_ByCustomer({ category_sublv3_id }))} />
                    <button type="button" onClick={() => setShowModal(true)} className="btn btn-sm btn-primary m-1" title="Add Data" data-toggle="modal" data-target="#modalAssignSLACustomer">
                        <Icons iconName="plus" className="svg-icon svg-icon-sm" /> Assign
                    </button>
                </CardToolbar>
            </CardHeader>
            <CardBody>
                {show_modal && <AssignModal category_sublv3_id={category_sublv3_id} />}

                <DataGrid
                    dataSource={assigned_customer_sla}
                    remoteOperations={true}
                    allowColumnReordering={true}
                    allowColumnResizing={true}
                    columnAutoWidth={true}
                    showBorders={true}
                    showColumnLines={true}
                    showRowLines={true}
                >
                    <HeaderFilter visible={true} />
                    <FilterRow visible={true} />
                    <Paging defaultPageSize={10} />
                    <Editing
                        refreshMode='reshape'
                        mode="row"
                        allowAdding={false}
                        allowDeleting={true}
                        allowUpdating={true}
                    />

                    <Pager
                        visible={true}
                        allowedPageSizes={[10, 20, 50]}
                        displayMode='full'
                        showPageSizeSelector={true}
                        showInfo={true}
                        showNavigationButtons={true} />
                    <Column caption="Actions" type="buttons">
                        <Button name="delete" cssClass="btn btn-icon btn-light-danger btn-sm mx-1">
                            <i className="fas fa-ban icon-sm"></i>
                        </Button>
                        <Button name="edit" cssClass="btn btn-icon btn-light-warning btn-sm mx-1">
                            <i className="fas fa-edit icon-sm"></i>
                        </Button>
                    </Column>
                    <Column caption="ID" dataField="id" allowEditing={false} visible={false} />
                    <Column caption="Customer Name" dataField="name" allowEditing={false} />
                    <Column caption="SLA (Days)" dataField="sla" />
                </DataGrid>
            </CardBody>
        </Card>
    )
}

export default AssignedCustomerSLA