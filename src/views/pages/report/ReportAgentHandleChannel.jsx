import React, { useEffect, useRef } from 'react'
import * as echarts from 'echarts';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import { Column, DataGrid, FilterRow, GroupPanel, HeaderFilter, Pager, Paging } from 'devextreme-react/data-grid'

import { Container, MainContent, SubHeader } from 'views/layouts/partials'
import { apiReportAgentHandleChannel, apiReportDataChannel } from 'app/services/apiReport';

function ReportByAgent() {
    const dispatch = useDispatch();
    const refDataChart = useRef();
    const { report_agent_handle_channel, report_data_channel } = useSelector(state => state.report);

    useEffect(() => {
        dispatch(apiReportAgentHandleChannel({ action: 'Today' }));
        dispatch(apiReportDataChannel({ action: 'Today' }));
    }, [dispatch]);

    useEffect(() => {
        function LoadDataChart() {
            const chartAgentHandle = echarts.init(refDataChart.current);
            const option = {
                legend: {
                    top: 'bottom'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                series: [{
                    name: 'Average Handling Time',
                    type: 'pie',
                    roseType: 'area',
                    center: ['50%', '50%'],
                    itemStyle: {
                        borderRadius: 8
                    },
                    data: []
                }]
            }

            let data_series = [];
            for (let i = 0; i < report_data_channel?.length; i++) {
                data_series.push({
                    name: report_data_channel[i].channel,
                    value: report_data_channel[i].total_handle,
                });
            }
            option.series[0].data = data_series;
            option && chartAgentHandle.setOption(option);
        }
        LoadDataChart()
    });

    async function handleDatetime(action) {
        dispatch(apiReportAgentHandleChannel({ action }))
        dispatch(apiReportDataChannel({ action }))
    }

    return (
        <MainContent>
            <SubHeader active_page="Reports" menu_name="Report" modul_name="Average Handling Time" />
            <Container>
                <div className="border rounded p-4 mb-4">
                    <div className="d-flex align-items-center justify-content-between">
                        <h4>Report Average Handling Time</h4>
                        <ul className="nav nav-pills nav-pills-sm nav-dark-75">
                            <li className="nav-item">
                                <Link to="#tab_7days" className="nav-link py-2 px-4" data-toggle="tab" onClick={() => handleDatetime('Last7Days')}>7 Days</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="#tab_today" className="nav-link py-2 px-4 active" data-toggle="tab" onClick={() => handleDatetime('Today')}>Today</Link>
                            </li>
                        </ul>
                    </div>

                    <div className="row my-4">
                        <div className="col-lg-6">
                            <DataGrid
                                dataSource={report_agent_handle_channel}
                                remoteOperations={{
                                    filtering: true,
                                    sorting: true,
                                    paging: true
                                }}
                                allowColumnReordering={true}
                                allowColumnResizing={true}
                                columnAutoWidth={true}
                                showBorders={true}
                                showColumnLines={true}
                                showRowLines={true}
                            >
                                <HeaderFilter visible={true} />
                                <FilterRow visible={true} />
                                <Paging defaultPageSize={20} />
                                <Pager
                                    visible={true}
                                    allowedPageSizes={[20, 50, 100]}
                                    displayMode='full'
                                    showPageSizeSelector={true}
                                    showInfo={true}
                                    showNavigationButtons={true} />
                                <GroupPanel visible={true} />
                                <Column caption="Agent Handle" dataField="agent_handle" />
                                <Column caption="Channel" dataField="channel" cellRender={(data) => {
                                    return <span className={`label label-md label-light-primary label-inline`}>{data.value}</span>
                                }} />
                                <Column caption="Total Handle" dataField="total_handle" />
                                <Column caption="Total Time" dataField="total_time" />
                                <Column caption="Avg. Time" dataField="avg_time" />
                            </DataGrid>
                        </div>
                        <div className="col-lg-6">
                            <div className="card border-0">
                                <div className="card-body p-0 bg-white" style={{ height: '400px', width: '100%' }} ref={refDataChart} />
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </MainContent>
    )
}

export default ReportByAgent