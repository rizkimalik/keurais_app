import React from 'react';
import MendawaiLogo from 'views/components/MendawaiLogo';

function Guest({ children }) {
    return (
        <div className="d-flex flex-column flex-root vh-100">
            <div className="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid">
                <div className="content order-2 order-lg-1 d-flex flex-column w-100 pb-0 mb-0" style={{ backgroundColor: '#17c7bd' }}>
                    <div className="d-flex flex-column justify-content-center text-center pt-md-5 px-lg-20 px-0 mt-10">
                        <h3 className="text-dark">Unify customer connections with Keürais Desk</h3>
                        <label className="text-dark">
                            Connect with your customers on every channel with our omnichannel contact center solution
                        </label>
                    </div>
                    <div className="d-flex flex-center my-10">
                        <img src="./assets/media/ticketing.png" alt="Logo-Ticketing" style={{ width: '600px', height: 'auto' }} />
                    </div>
                </div>

                <div className="login-aside order-1 order-lg-2 d-flex flex-row-auto position-relative overflow-hidden" style={{ backgroundColor: '#eee' }}>

                    <div className="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
                        <div className="d-flex flex-column-fluid flex-column flex-center">
                            <div className="text-center">
                                <MendawaiLogo colorLogo="black" className="max-h-60px" />
                            </div>
                            <div className="login-form login-signin py-11">
                                {children}
                            </div>
                        </div>
                        <div className="d-flex justify-content-center align-items-center py-5">
                            <span>Copy Right © Keurais Team 2023</span>
                        </div>
                        <div className="d-flex justify-content-center align-items-center py-7 py-lg-0">
                            <a href="https://home.keurais.com/privacy-policy" target="_blank" className="text-primary ml-5 font-weight-bolder font-size-lg">Privacy Policy</a>
                            <a href="https://home.keurais.com" target="_blank" className="text-primary ml-5 font-weight-bolder font-size-lg">About Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Guest
